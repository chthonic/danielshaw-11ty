const MiniCssExtractPlugin = require( 'mini-css-extract-plugin' );
const path = require( 'path' );
const { WebpackManifestPlugin } = require('webpack-manifest-plugin');

const production = 'development' !== process.env.NODE_ENV;

module.exports = {
	entry: {
		bundle: './develop/assets/src/js/bundle.js',
		style: './develop/assets/src/js/style.js'
	},
	output: {
		library: 'SITE',
		path: path.resolve( __dirname, 'develop/assets/dist' ),
		publicPath: '',
		filename: 'js/[name].[contenthash].js',
	},
	module: {
		rules: [
			{
				test: /\.scss$/,
				use: [
					MiniCssExtractPlugin.loader,
					{
						loader: 'css-loader',
						options: {
							url: false,
						},
					},
					{
						loader: 'postcss-loader',
						options: { 
							postcssOptions: {
								ident: 'postcss',
							}
						},
					},
					{
						loader: 'sass-loader',
						options: {
							implementation: require('sass'),
							sassOptions: {
								outputStyle: 'expanded',
							},
						},
					},
				],
			},
			{
				test: /\.js$/,
				use: 'babel-loader',
				exclude: /(node_modules)/,
			}
		],
	},
	optimization: {
		splitChunks: {
			cacheGroups: {
				commons: {
					test: /[\\/]node_modules[\\/]/,
					name: 'vendor',
					chunks: 'all',
				},
			},
		},
	},
	plugins: [
		new MiniCssExtractPlugin( {
			filename: 'css/[name].[contenthash].css',
		} ),
		new WebpackManifestPlugin(),
	],
};

if ( production ) {
	module.exports.devtool = false;
}