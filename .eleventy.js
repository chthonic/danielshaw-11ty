// Import filters
const filterDateBot = require('./develop/_filters/filter-date-bot.js');
const filterDateHuman = require('./develop/_filters/filter-date-human.js');
const filterAssetPath = require('./develop/_filters/filter-asset-path.js');
const filterPreventOrphans = require('./develop/_filters/filter-prevent-orphans.js');

// Import transforms
const transformHtmlMin = require('./develop/_transforms/transform-minify-html.js');

module.exports = function(config) {
	// Configure template language
	config.setLiquidOptions({
		dynamicPartials: true,
		strictFilters: true,
	  });

	config.setTemplateFormats([
		'html',
		'jpg',
		'png',
		'webp',
		'svg',
		'ico',
		'woff',
		'woff2'
	])

	//
	config.setUseGitIgnore(false);

	// Watch dist assets for change
	config.addWatchTarget('./develop/assets/dist/css/style.css');
	config.addWatchTarget('./develop/assets/dist/js/bundle.js');

	// Copy files directly to output folder
	config.addPassthroughCopy({ 'develop/assets/dist': 'assets' });
	config.addPassthroughCopy({ '.htaccess': '.htaccess' });

	// Filters
	config.addFilter('dateBot', filterDateBot);
	config.addFilter('dateHuman', filterDateHuman);
	config.addFilter('assetPath', filterAssetPath);
	config.addFilter('preventOrphans', filterPreventOrphans);

	// Transforms
	config.addTransform('htmlmin', transformHtmlMin);

	return {
		dir: {
			input: 'develop',
			output: 'release'
		},
		passthroughFileCopy: true
	};
};
