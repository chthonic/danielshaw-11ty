const site = {
	"identity": "Daniel Shaw",
	"domain": "danielshaw.co.nz",
	"url": "https://danielshaw.co.nz",
	"logo": "https://danielshaw.co.nz/assets/image/logo-daniel-shaw-wordpress-developer.png",
	"email": "daniel@chthonic.nz",
	"notice": "Not available for new client work right now!",
	"phone": "020 414 82576",
	"location": "Wellington, New Zealand",
	"wordmark": "Daniel Shaw · WordPress &amp; WooCommerce Developer"
};

module.exports = function () {
	if ('development' === process.env.ELEVENTY_ENV) {
		site.url = 'http://localhost:8081';
		site.logo = 'http://localhost:8081/assets/image/logo-daniel-shaw-wordpress-developer.png';
	}

	return site;
}