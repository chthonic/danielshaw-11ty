const htmlmin = require('html-minifier');

module.exports = function htmlMinTransform(htmlContent, outputPath) {
  if (outputPath.endsWith('.html')) {
    let minified = htmlmin.minify(htmlContent, {
      removeComments: true,
      collapseWhitespace: true,
      minifyCSS: true,
      minifyJS: true,
      processScripts: ['application/ld+json']
    });

    return minified;
  }
  
  return htmlContent;
};
