const fs = require('fs');
const path = require('path');

module.exports = value => {
	const developAssetPath = 'develop/assets/dist';
	const releaseAssetPath = 'assets';

	const manifestPath = path.resolve(
		developAssetPath,
		'manifest.json'
	);
	
	const manifest = JSON.parse(fs.readFileSync(manifestPath));
	return `/${releaseAssetPath}/${manifest[value]}`;
};