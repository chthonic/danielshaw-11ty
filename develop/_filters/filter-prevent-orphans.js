module.exports = title => {
  const words = title.split(' ');
  const endWords = [words.pop(), words.pop()];
  return (
    words.join(' ') +
    (endWords[0].length + endWords[1].length < 10 ? '&nbsp;' : ' ') +
    endWords[1] +
    '&nbsp;' +
    endWords[0]
  );
};