---
layout: templates/post.html
title: How to survive the (WP) eCommerce experience
date: 2014-05-26
lastModified: 2014-05-26
tags: ['note']
type: e-commerce
permalink: 'how-to-survive-the-wp-ecommerce-experience/'
metaDescription: Tips for working with WP eCommerce, plus some general considerations when developing an eCommerce store.
hasBreadcrumb: true
---

<p>It's been seven years since I first installed WP eCommerce. The hope of a quick solution for adding an e-commerce component to an existing WordPress site was destroyed fairly quickly.</p>

<h2>Colouring outside the lines</h2>

<p>If you're familiar with WP eCommerce then it's pretty safe to assume you're familiar with the amount of grief the developers have endured over the years. The sentiment is not totally mis-directed (for a great recent example check out <a href="https://wordpress.org/support/topic/spam-users-in-wp_users-after-wpsc-upgrade">this major hiccup</a>), but if you look through the majority of complaints in <a href="https://wordpress.org/support/plugin/wp-e-commerce">the forums</a> it's evident you need to have been around the block with WordPress at least a couple of times to get the best results.</p>

<p>So, apart from some basic WP nous, on an install-and-forget level it can be as solid as any other WordPress plugin having a good day.</p>

<p>Does that make the title of this post seem a little extreme? Perhaps... but only if you've never had to deal with a special shipping request, or tried to tidy up the checkout template, or even just tried to make sense of how AJAX has been implemented. This is, of course, endemic to all plugins to some degree—promise and expectation do not match up with a project's specific requirements—but when trying to extend WP eCommerce often the issue is more to do with how it does it, rather than what it does.</p>

<h2>Expectation vs. reality</h2>

<p>As with many things where there are practical considerations, siding with reality as early as possible can be very rewarding! To achieve this it's <span class="line-through">a good idea</span> imperative to begin with the backbone of any e-commerce project: determine your shipping method and product meta requirements right at the beginning and you will be in for fewer desperate, last-minute re-jigs, I promise.</p>

<p>For almost every online store I've worked on for a 3rd-party these two elements are often considered long after development has begun, if not last of all. Product meta, in particular, can be responsible for driving content on almost every page of an e-commerce site, and, if you're not taking it as a starting point to determine the shape of your site, you're pretty much choosing to work against the flow of content.</p>

<h2>Product first</h2>

<p>A lot of projects start with the home page and then move on to internal pages. I guess this is down to the home page being the easiest concept to sell to a client, particularly because it tends to hold all of the aesthetically-pleasing / web kitsch elements (choose your side!). Myself, I feel home pages are a quality dumping ground for "important marketing ideas", image sliders and the like. Sure, you can still have a pretty home page, but since it's essentially worthless it does not make much sense to start with it. It's an easy, pretty treat to finish with and something shiny to look at when it's all over.</p>

<p>For an e-commerce site, especially, dedicating this time instead to determining the actual details of a product page will reward you with a stronger relationship between content and site structure. Think about it this way: the product page is a content engine pushing itself in some form to almost every other page on your site, meaning once you have the details of your product locked down the rest of the site will build itself (well, not really, but hopefully you see the benefit here).</p>

<p><strong>Moral of this section:</strong> use real product content right from the beginning because dummy placeholders mean assumption plus deferred additional work.</p>

<h2>Delivering the goods</h2>

<p>First and best option: offer free shipping, either absorbing the cost or building it into your price structure. Free shipping has gained a lot of traction as a selling point these days, anyway, so if at all an option it's definitely an excellent one to choose.</p>

<p>Bonus: you suddenly have an additional marketing feature, and in the long-term you may find going for the simpler option actually saves you cash.</p>

<p>Of course, sadly for both the developer and site owner, this is not always an available option. You'll need to choose some form of measurement to determine shipping costs against — size, weight and number of products are all common models for measuring shipping costs, easy!</p>

<p>Next, you're going to decide that if a certain amount is spent the shipping will be free — but, then, this will only apply to round objects that weigh less than 1kg (but more than 200 grams). And, they must be blue, otherwise the amount spent will need to be 10% more&hellip;</p>

<p>Or, perhaps you've decided to go with a flat rate based on region, but at the last moment you realise you'd prefer to use a postal provider's <a href="http://www.nzpost.co.nz/products-services/iphone-apps-apis/rate-finder-api">Rate Finder API</a> to provide customers with an actual cost — great! — but now you'll need to go back and add dimensions and weight data to all of your products&hellip;</p>

<p>Shipping can become complex very quickly if you don't make a plan early on.</p>

<h2>Wrangling WP <span style="text-transform:lowercase;">e</span>Commerce</h2>

<p>Probably the best advice I can give is, do not use the WP eCommerce styles. Apart from breaking a responsive site, the CSS will hurt your precious brain if you're required to make anything beyond trivial changes to presentation. Instead, deregister it and use your own styles.</p>

<p>To deregister, add the following to your theme's functions.php:</p>
<!-- htmlmin:ignore -->
<div class="code-block">
function deregister_styles() {
    if ( !is_admin() ) {
        wp_deregister_style( 'wpsc-theme-css' );
        wp_deregister_style( 'wpsc-theme-css-compatibility' );
        wp_deregister_style( 'wp-e-commerce-dynamic' );
    }
}

add_action( 'wp_print_styles', 'deregister_styles', 100 );
</div>
<!-- htmlmin:ignore -->
<p>Additionally, the only conceivable reason to purchase the Gold Cart module is to use one of the payment gateways not included with the free version. If you do install Gold Cart for this reason, don't forget to also deregister the separate Gold Cart styles:</p>
<!-- htmlmin:ignore -->
<div class="code-block">
wp_deregister_style( 'wpsc-gold-cart' );
</div>
<!-- htmlmin:ignore -->
<p>With regards to scripts loaded by the plugin you can obviously replace those as well. However, this can lead to broken functionality in the admin screens; basically, a client will see an option, change it and then wonder why it does nothing. Typically, I'll leave the scripts as-is unless I'll be closely managing the site myself. One exception is the long-redundant Thickbox script, now native to WordPress:</p>
<!-- htmlmin:ignore -->
<div class="code-block">
function deregister_script() {
    if ( ! is_admin() ) {
        wp_deregister_script( 'wpsc-thickbox' );
    }
}

add_action( 'wp_print_scripts', 'deregister_script', 100 );
</div>
<!-- htmlmin:ignore -->