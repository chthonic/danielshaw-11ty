---
layout: templates/post.html
title: Simple slider for WordPress Post Types with Transients
date: 2014-10-31
lastModified: 2014-10-31
tags: ['note']
type: wordpress
permalink: 'simple-post-slider-using-transients-api/'
metaDescription: A short and sweet approach to creating and caching an image slider for a WordPress post, page or custom post type.
hasBreadcrumb: true
---
<p>I've been using the <a href="http://codex.wordpress.org/Transients_API">Transients API</a> on a more regular basis recently, what with Optimise! in the wind so much right now. Here's a short & sweet approach to creating and caching an image slider for a WordPress post, page or custom post type.</p>

<p>For this example, the result would be an image slider with thumbnail pagination (per the layout <a href="http://bxslider.com/examples/thumbnail-pager-1">here</a>).</p>

<h2>BXin'</h2>

<p><a href="http://bxslider.com/">bxSlider</a> is a really great script that comes with a lot of helper options built-in. The markup is a simple unordered list containing images, with a class on the &lt;ul&gt; tag.</p>

<p>Super-flexible situation.</p>

<p>The bundled CSS can be trimmed substantially—obviously this depends on your particular configuration—and, once done, it can be dropped into style.css to avoid an additional script link.</p>

<p>For JS dependencies on WordPress sites you are best served linking javascript files via the <a href="http://codex.wordpress.org/Function_Reference/wp_register_script">wp_register_script</a> (for scripts with handles pre-registered in WordPress, only) and <a href="http://codex.wordpress.org/Function_Reference/wp_enqueue_script">wp_enqueue_script</a> functions. So, for bxSlider:</p>
<!-- htmlmin:ignore -->
<div class="code-block">
function the_js() {
    if ( !is_admin() ) {
        wp_register_script( 'jquery' );
        wp_enqueue_script(
            'bx-slider',
            get_template_directory_uri() . '/js/jquery.bxslider.min.js',
            array( 'jquery-core' ),
            4.1.2,
            true
        );
    }
}

add_action( 'wp_enqueue_scripts', 'the_js' );
</div>
<!-- htmlmin:ignore -->
<p>Lastly, call the slider via the &lt;ul&gt; class:</p>
<!-- htmlmin:ignore -->
<div class="code-block">
jQuery(document).ready(function($) {
    $('.bx-slider').bxSlider();
} );
</div>
<!-- htmlmin:ignore -->
<h2>Image bank</h2>

<p>The way I typically use this, the images are all stored as attachments to the current page. For the client, it's then just a case of uploading an image via the post Media Uploader in order to create or add to a slider.<sup><a id="note-1-return" href="#note-1">1</a></sup></p>

<p>To control the output and avoid image dimension insanity<sup><a id="note-2-return" href="#note-2">2</a></sup>, set up some resizing rules with <a href="http://codex.wordpress.org/Function_Reference/add_image_size">add_image_size</a>.</p>
<!-- htmlmin:ignore -->
<div class="code-block">
if ( function_exists( 'add_image_size' ) ) {

    add_image_size( 'bx-slide', 500, 500, true );
    add_image_size( 'bx-slide-pager', 75, 75, true );

}
</div>
<!-- htmlmin:ignore -->
<p>The above will create two additional images per uploaded image, one for the slider and one for its corresponding pagination thumbnail.</p>

<h2>All together now</h2>

<p>To pull the post images into the slider use <a href="http://codex.wordpress.org/Function_Reference/get_children">get_children</a>. This will output an array which can then be looped through to grab each image:</p>
<!-- htmlmin:ignore -->
<div class="code-block">
$images = get_children( array(
        'post_parent'    => $post->ID,
        'post_status'    => 'inherit',
        'post_type'      => 'attachment',
        'post_mime_type' => 'image',
        'order'          => 'ASC',
        'orderby'        => 'menu_order'
    )
);

foreach ( $images as $image ) {
    $img = wp_get_attachment_image (
        $image->ID,
        'bx-slide',
        false,
        array( 'alt' => 'Something alty' )
    );

    echo $img;
}

// Output: &lt;img src="http://example.com/slide.jpg" alt="Something alty" /&gt;
</div>
<!-- htmlmin:ignore -->
<p>The important part is the second argument of wp_get_attachment_image: bx-slide. This references the resized images defined earlier with add_image_size and ensures a correctly proportioned image is used.</p>

<p><strong>A couple of useful things to note:</strong></p>

<ol>
    <li>setting <code>'orderby' => 'menu_order'</code> as an argument of <code>get_children</code> allows the slider image order to be manipulated via the drag-and-drop Media Uploader panel</li>
    <li>you can add additional attributes to the <code>&lt;img&gt;</code> element with an optional array as the fourth argument of <code>wp_get_attachment_image()</code>.</li>
</ol>

<p>At this point, it's just a case of wrapping everything with the bxSlider markup to unlock and achieve your image slider badge.</p>

<h2>Transientify!</h2>

<p>OK.</p>
<!-- htmlmin:ignore -->
<div class="code-block">
function slide_images() {
    global $post;

    $is_transient = 'slide_images_' . $post->ID;<sup><a id="note-3-return" href="#note-3">3</a></sup>
    $slide_images = get_transient( $is_transient );

    if ( false === $slide_images ) {
        ob_start();

        $images = get_children( array(
                'post_parent'    => $post->ID,
                'post_status'    => 'inherit',
                'post_type'      => 'attachment',
                'post_mime_type' => 'image',
                'order'          => 'ASC',
                'orderby'        => 'menu_order'
            )
        );

        echo '&lt;ul class="bx-slider"&gt;';

        foreach ( $images as $image ) {
            $img = wp_get_attachment_image(
                $image->ID,
                'bx-slide',
                false,
                array( 'alt' => get_the_title() )
            );

            echo '&lt;li&gt;' . $img . '&lt;/li&gt;';
        }

        echo '&lt;/ul&gt;';
        echo '&lt;div id="bx-pager"&gt;';

        $count = 0;

        foreach ( $images as $image_thumb ) {
            $thumb = wp_get_attachment_image(
                $image_thumb->ID,
                'bx-slide-pager',
                false
            );

            echo '&lt;a data-slide-index="' . $count . '" href=""&gt;' . $thumb . '&lt;/a&gt;';

            $count++;
        }

        echo '&lt;/div&gt;';

        $slide_images = ob_get_contents();
        ob_end_clean();

        set_transient( $is_transient, $slide_images, 60*60 );
    }

    echo $slide_images;
}
</div>
<!-- htmlmin:ignore -->
<p><strong>We are:</strong></p>

<ul>
    <li>setting a unique name for the transient by appending a unique value (the post ID) to a prefix</li>
    <li>checking to see if a transient for this post type object already exists</li>
    <li>if a transient exists go to the last line</li>
    <li>if there is no transient, run the query</li>
    <li>set the output of the query in a transient</li>
    <li>output the transient</li>
</ul>

<p>The third argument of <code>set_transient()</code>, "60*60", specifies how long a transient should persist. In this case it will expire after one hour (60 x 60 seconds). Once a transient has expired a new one will be created when the page is next visited.</p>

<p>Back on your page, you just need to call <code>slide_images()</code>&hellip;</p>
<!-- htmlmin:ignore -->
<div class="code-block">
&lt;?php slide_images(); ?&gt;
</div>
<!-- htmlmin:ignore -->
<p>&hellip;and your slider will display.</p>

<h2>Clearing the cache</h2>

<p>As soon as your slider displays in a browser for the first time, it will be cached. However, you may need to make a change—for example, add a new image—and want to see the result without waiting for the transient to expire. In this case, just hook <code>delete_transient()</code> into the <code>save_post</code> action, as so:</p>
<!-- htmlmin:ignore -->
<div class="code-block">
function slide_images_clear() {
    delete_transient( 'slide_images_' . $post->ID );
}

add_action( 'save_post', 'slide_images_clear' );
</div>
<!-- htmlmin:ignore -->
<p>With the above in place, whenever content is saved the associated transient will be expired.</p>

<div class="footnotes">
    <ol>
        <li id="note-1">So, caveat: if there's a requirement for images on a page other than those to be included in the slider, there'll need to be additional filtering for "slider only" images going on.<a href="#note-1-return"><i class="fa fa-reply"></i></a></li>
        <li id="note-2">The most amazing I've seen so far was a client-uploaded image with a 1:20 ratio.<a href="#note-2-return"><i class="fa fa-reply"></i></a></li>
        <li id="note-3">Don't forget to <a href="http://benfrost.bandcamp.com/track/untitled-transient">title your transients</a>!<a href="#note-3-return"><i class="fa fa-reply"></i></a></li>
    </ol>
</div>
