---
layout: templates/post.html
title: Dynamic search filter for WordPress categories
date: 2014-06-09
lastModified: 2014-06-09
changeFreq: yearly
tags: ['note']
type: wordpress
permalink: 'dynamic-search-filter-for-wordpress-categories/'
metaDescription: How to set up a multi-select search filter for WordPress categories.
hasBreadcrumb: true
---
<p>A common scenario: you have a bunch of categories, say, Label > Artist > Track. Following is an approach for a hierarchical category search filter on your WordPress site.</p>

<h2>The basics</h2>

<p>Here is the requirement:</p>

<ul>
    <li>A form with three select elements to reflect this hierarchy:<br />
    <code>Label > Artist > Release</code></li>
    <li>Choosing an option from a preceding select element dynamically populates the following select element</li>
</ul>

<h2>Category structure</h2>

<p>The category hierarchy looks something like this:</p>

<p><code>Music > Label > Artist > Release</code></p>

<p><code>Music</code> is really just a container to keep everything tidy and discrete. As the top-most parent category it will generate the contents for the first child select element, <code>Label</code>.</p>

<p><code>Label</code>, <code>Artist</code> and <code>Release</code> are not actual categories but describe the type of category contained in each select element; the immediate children of the top-most <code>Music</code> category will be music label names, the grand-children will be musician names, and so on. For example:</p>

<ul>
    <li><code>Label: Raster Noton > Artist: Kyoka > Release: Ufunfunfufu</code></li>
</ul>

<p>The key to this approach is to use <a href="http://codex.wordpress.org/Function_Reference/wp_dropdown_categories">wp_dropdown_categories()</a> to generate each select element. Whenever an option is selected we can use this function's child_of argument to dynamically generate the following select element's contents.</p>

<p>The following will generate the first select element for categories of type <code>Label</code>:</p>
<!-- htmlmin:ignore -->
<div class="code-block">
wp_dropdown_categories(
    array(
        'child_of'        => 1,
        'depth'           => 1,
        'hide_empty'      => 0
        'hierarchical'    => 1,
        'id'              => 'label',
        'name'            => 'label',
        'orderby'         => 'NAME',
        'show_option_all' => 'All Labels',
    )
);
</div>
<!-- htmlmin:ignore -->
<p><strong>Tip:</strong> set <code>'hide_empty'</code> to 0 during development if you want to see more than empty select elements. If this argument is not set you'll need to assign categories to posts to make them visible.</p>

<p>Regarding the above example:</p>

<ul>
    <li><code>'child_of'</code> has a value of 1, the category ID of the <code>Music</code> category I'm using here. Use whichever method you prefer to find the top-most category ID for your set of terms.</li>
    <li>Both <code>'depth'</code> and <code>'hierarchical'</code> keys are given a value of 1 to ensure only the immediate children are included.</li>
    <li>Additional select elements for <code>Artist</code>- and <code>Release</code>-type categories can be generated in the same way, the only difference being a change in value for the <code>'child_of'</code> key.</li>
</ul>

<p>The next step is to set up an AJAX request to return contextual select elements based on user selection.</p>

<h2>Switched at birth</h2>

<p>Here is what needs to happen:</p>

<ul>

    <li>Capture the value whenever a <code>select</code> element changes</li>
    <li>AJAX the value to a custom function</li>
    <li>Return a new <code>wp_dropdown_categories()</code> object</li>
    <li>Update the next <code>select</code> element</li>

</ul>

<h2>Listen / request / receive</h2>

<p>Listening for change and making an AJAX request with jQuery:</p>
<!-- htmlmin:ignore -->
<div class="code-block">
$( document ).on( 'change', '#label', function() {
    var label = $( '#label' ).val();

    $.ajax({
        url: "http://example.com/wp-admin/admin-ajax.php",
        type: 'POST',
        data: 'action=update_label&label=' + label
        success: function(results) {
            $( '#artist' )
            .replaceWith( results );
        }
    });
});
</div>
<!-- htmlmin:ignore -->
<p>If you're not overly familiar with WordPress, the URL set in the request above points to WordPress' native implementation of AJAX. Even though it's called <code>admin-ajax</code> it can be used for both back- and front-end requests.</p>

<h2>Update the child</h2>

<p>The AJAX request posts a value to a custom function which will construct a new child select element. The function is very simple, as simple as setting the passed <code>Label</code> value as the value of child_of:</p>
<!-- htmlmin:ignore -->
<div class="code-block">
function label_filter() {
    if ( isset( $_POST['label'] ) ) {
        $is_label = $_POST['label'];

        wp_dropdown_categories(
            array(
                'name'            => 'artist',
                'id'              => 'artist',
                'orderby'         => 'NAME',
                'hierarchical'    => 1,
                'child_of'        => $is_label,
                'depth'           => 1,
                'show_option_all' => 'Select...'
            )
        );
    }

    die();
}

add_action( 'wp_ajax_update_label', 'label_filter' );
add_action( 'wp_ajax_nopriv_update_label', 'label_filter' );
</div>
<!-- htmlmin:ignore -->
<p>Each action uses the native <a href="http://codex.wordpress.org/Plugin_API/Action_Reference/wp_ajax_(action)">wp_ajax_</a> action hook to trigger the success attribute of the request. Looking back at the data attribute of the request&hellip;</p>
<!-- htmlmin:ignore -->
<div class="code-block">
data:'action=update_label&label=' + label
</div>
<!-- htmlmin:ignore -->
<p>&hellip; you'll notice the value of the action attribute—<code>update_label</code>—is appended to <code>wp_ajax_</code>. The first time I worked with this hook I didn't follow this convention and consequently never received a success value.</p>

<p>The other thing to note is the fact there is two actions: one is for logged-in users, the other for non-logged-in users.</p>

<p>At this point the basic moving parts for the search filter are in place—sans sanity and security checks. There's also need for additional logic to initially disable all select children, and enable a child whenever its immediate parent changes.</p>
