---
layout: templates/post.html
title: Easier subtitles &amp; captions for video in the WordPress block editor
date: 2020-11-01
lastModified: 2020-11-05
tags: ['note']
type: wordpress
permalink: "video-tracks-wordpress/"
metaDescription: A.
hasBreadcrumb: true
---
<p>WordPress 5.6 is set for release on December 8 2020, bringing native support 
for text tracks added via the Video block. Previously, supplementing video with 
a transcript has been a multi-step opaque process from within WordPress.</p>

<p>The benefit of this update will possibly be broader than simply making this 
task easier. Hopefully, more sites will start to supplement video content with 
transcripts simply because the mechanism to do so will be much more visible. 
Adding a transcript is beneficial for general accessibility—for example, anyone 
unable to hear audio content for personal or environmental factors—and for SEO: 
Google will happily suck up that text aligned with your video media.

<h2>What is a video “text track”?</h2>

<p>Broadly, a text track can be a transcript of language, on-/off-screen 
sounds, or time-based and contextual metadata, depending on the intended audience. 
A text track is assigned to a <code>&lt;track&gt;</code> element, as a child of a 
<code>&lt;video&gt;</code> element:</p>
<!-- htmlmin:ignore -->
<div class="code-block">
&lt;video src="video.mp4" controls&gt;
  &lt;track src="caption.vtt"
    label="Title for track"
    srclang="en"
    kind="captions"&gt;
&lt;/video&gt;
</div>
<!-- htmlmin:ignore -->
<p>Using the block editor, the Video block automatically wraps <code>&lt;video&gt;</code> 
in a <code>&lt;figure&gt;</code> tag:</p>
<!-- htmlmin:ignore -->
<div class="code-block">
&lt;figure class="wp-block-video"&gt;
  &lt;video src="video.mp4" controls&gt;
    &lt;track src="caption.vtt"
    label="Title for track"
    srclang="en"&gt;
  &lt;/video&gt;
&lt;/figure&gt;
</div>
<!-- htmlmin:ignore -->

<h2>How to add tracks to the Video block</h2>

<p>One or multiple tracks can be assigned to a track via the Text tracks dropdown 
in the Video block toolbar:</p>

<figure>
  <picture>
    <source srcset="{{ site.url }}/assets/image/wordpress-video-block-text-tracks-L.webp" media="(min-width: 60em)">
    <source srcset="{{ site.url }}/assets/image/wordpress-video-block-text-tracks-S.webp" media="(max-width: 59.96875em)">
    <img src="{{ site.url }}/assets/image/wordpress-video-block-text-tracks.jpg"
      height="436"
      width="675"
      alt=""
      loading="lazy"
    />
  </picture>
  <figcaption>The interface for adding a text track.</figcaption>
</figure>

<p>For each text track, the <code>label</code>, <code>source language</code> and 
<code>kind</code> attributes can be set:</p>

<figure>
  <picture>
    <source srcset="{{ site.url }}/assets/image/wordpress-video-block-text-tracks-attributes-L.webp" media="(min-width: 60em)">
    <source srcset="{{ site.url }}/assets/image/wordpress-video-block-text-tracks-attributes-S.webp" media="(max-width: 59.96875em)">
    <img src="{{ site.url }}/assets/image/wordpress-video-block-text-tracks-attributes.jpg"
      height="470"
      width="675"
      alt=""
      loading="lazy"
    />
  </picture>

  <figcaption>Text track with attributes assigned.</figcaption>
</figure>

<h3>Label</h3>
<p>A simple title for the track.</p>

<h3>Source language</h3>
<p>The language of the track text (must be specified in <a href="https://en.wikipedia.org/wiki/IETF_language_tag" rel="nofollow">IETF BCP 47 language tag</a> format).</p>

<h3>Kind</h3>
<ul class="list-highlight" role="list">
  <li><b>Captions:</b> help audio-impaired (due to personal or environmental factors) viewers follow audio events on- and off-screen</li>
  <li><b>Subtitles:</b> help viewers unfamiliar with the audio language</li>
  <li><b>Descriptions:</b> akin to an audio alt text, when the primary visual material is not available for some reason</li>
  <li><b>Chapters:</b> intended to help navigation within a video</li>
  <li><b>Metadata:</b> supplies time-based data for use within the DOM <sup><a id="note-1-ref" href="#note-1" aria-describedby="footnote-label">1</a></sup></li>
</ul>

<p>At time of writing, if you choose Subtitles the <code>kind</code> attribute will be 
omitted from the <code>&lt;track&gt;</code> markup: 
<a href="https://github.com/WordPress/gutenberg/issues/26673" rel="nofollow">#26673</a></p>
 

<h2>How to create a text track</h2>

<p>Text tracks are created in Web Video Text Tracks (WebVTT) format. This basic subtitle example shows the key requirements:</p>
<!-- htmlmin:ignore -->
<div class="code-block">
WEBVTT

01:41:16.208 --> 01:41:18.506
- Why don't we just...

01:41:19.778 --> 01:41:22.770
- wait here for a little while?

01:41:24.450 --> 01:41:26.418
- See what happens.
</div>
<!-- htmlmin:ignore -->
<ul>
  <li>The file must start with WEBVTT</li>
  <li>Timecodes can be either HH:MM:SS.TTT or MM:SS.TTT format <sup><a id="note-2-ref" href="#note-2" aria-describedby="footnote-label">2</a></sup></li>
  <li>Note the period between seconds and milliseconds</li>
  <li>The hyphen preceding each text block seems to be convention and not a hard 
  requirement. It will be displayed during playback.</li>
  <li>The saved file will have a .vtt extension.
</ul>

<p>From a former life working with timecodes and subtitles, I can attest to how 
an incorrect timecode will often introduce a lovely cascade effect impacting 
surrounding timecodes. A <a href="https://quuz.org/webvtt/">.vtt validator</a>
will likely always help avoid excessive debug time.</p>

<div class="footnotes">
  <h2 id="footnote-label" class="visually-hidden">Footnotes</h2>

  <ol>
    <li id="note-1"><a href="https://www.w3.org/wiki/VTT_Concepts#Different_types_of_VTT_tracks_and_their_structures">Different types of VTT tracks and their structures</a> <a href="#note-1-ref" aria-label="Back to content">↩</a></li>
    <li id="note-2">Hours:Minutes:Seconds.Milliseconds <a href="#note-2-ref" aria-label="Back to content">↩</a></li>
  </ol>
</div>