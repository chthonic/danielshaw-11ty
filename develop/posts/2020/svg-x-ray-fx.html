---
layout: templates/post.html
title: An interactive X-ray effect for SVG illustration
date: 2020-11-24
lastModified: 2020-11-24
tags: ['note']
type: fx
permalink: "x-ray-effect-svg/"
metaDescription: How to set up a user-driven X-ray effect for your SVG illustration.
hasBreadcrumb: true
---
<p>I&rsquo;m a fan of the interactive illustration present on sites like 
<a href="https://robbowen.digital/" rel="nofollow">robbowen.digital</a> and
<a href="https://www.cassie.codes/" rel="nofollow">cassie.codes</a>, specifically
the self-portraits. Recently, I began an ongoing makeover for this site, including a
refresh of my own little <a href="{{ site.url }}/">selfie icon</a>.</p>

<p>I originally imagined I&rsquo;d set up something similar to the user-animated
illustrations in the above links. Instead, I chose to continue the half-assed visual
theme from my previous version of this site: surface skin versus underlying structure,
the axis of activity for a front-end developer.</p>

<p>Here&rsquo;s the approach I took to create an illustrated selfie with interactive
x-ray effect, radiation-free!</p>

<h2>Surface and structure</h2>
<p>I feel some regret I&rsquo;d already resolved the selfie icon before thinking
about a bone layer. Life drawing teachers everywhere collectively sigh. Let&rsquo;s
pretend I purposely took the cartoon-first approach of the Korean artist,
<a href="https://www.hyungkoolee.com/portfolio/lepus-animatus/" rel="nofollow">Hyungkoo Lee</a>:</p>

<div class="columns">
	<div class="column pad-small-all">
		<figure>
      {% include './assets/image/svg-x-ray-fx/selfie.svg' %}
			<figcaption>The soft &amp; fleshy surface</figcaption>
		</figure>
	</div>
	
	<div class="column pad-small-all">
		<figure>
      {% include './assets/image/svg-x-ray-fx/selfie-skull.svg' %}
			<figcaption>The crunchy interior</figcaption>
		</figure>
	</div>
</div>

<p>These sit in a single <code>SVG</code> element where, by default, the bone layer 
will visually overlap the skin layer. A mask element can help manage when the bone layer
should be revealed. Here&rsquo;s some fairly standard SVG markup at this stage:</p>
<!-- htmlmin:ignore -->
<div class="code-block">
&lt;svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 350 500"&gt;
  &lt;g class="surface"&gt;
    &lt;path class="skin" .../&gt;
  &lt;/g&gt;
  &lt;g class="structure"&gt;
    &lt;path class="bone" .../&gt;
  &lt;/g&gt;
&lt;/svg&gt;
</div>
  <!-- htmlmin:ignore -->

<h2>Creating and referencing an SVG mask</h2>

<p>First up, here&rsquo;s what the basic mask markup looks like:</p>
<!-- htmlmin:ignore -->
<div class="code-block">
&lt;defs&gt;
  &lt;clipPath id="mask"&gt;
    &lt;circle cx="0" cy="0" r="20%"&gt;
  &lt;/clipPath&gt;
&lt;/defs&gt;
</div>
<!-- htmlmin:ignore -->
<p>It&rsquo;s composed of two SVG elements: a <a href="https://developer.mozilla.org/en-US/docs/Web/SVG/Element/circle"><code>&lt;circle&gt;</code></a> 
and <a href="https://developer.mozilla.org/en-US/docs/Web/SVG/Element/clipPath"><code>&lt;clipPath&gt;</code></a>.
A circle is simple and makes visual sense for this project but any complex shape
can be used as a mask. The <code>&lt;clipPath&gt;</code> element assigns the circle
as the mask shape for the bone layer.</p>

<p>A <code>&lt;circle&gt;</code> is a vector shape, one of three graphical object
types allowable within an SVG: vector, bitmap, and text. Wrapping the entire mask
definition in a <code>&lt;defs&gt;</code> element provides control over how it
will be displayed, rather than automatically being rendered as a visual element.</p>

<p>The mask is applied to the bone layer by wrapping the relevant <code>&lt;path&gt;</code>
elements with a <code>&lt;g&gt;</code>, with the mask referenced by ID:</p>
<!-- htmlmin:ignore -->
<div class="code-block">
&lt;g clip-path="url(#mask)" class="structure"&gt;
  &lt;path class="bone" .../&gt;
&lt/g&gt;
</div>
<!-- htmlmin:ignore -->
<p>The final structure of the prepped SVG will look like this:</p>
<!-- htmlmin:ignore -->
<div class="code-block">
&lt;svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 350 500"&gt;
  &lt;defs&gt;
    &lt;clipPath id="mask"&gt;
      &lt;circle class="mask-shape" cx="0" cy="0" r="20%"/&gt;
    &lt;/clipPath&gt;
  &lt;/defs&gt;
  &lt;g class="surface"&gt;
    &lt;path class="skin" .../&gt;
  &lt;/g&gt;
  &lt;g clip-path="url(#mask)" class="structure"&gt;
    &lt;path class="bone" .../&gt;
  &lt;/g&gt;
&lt;/svg&gt;
</div>
<!-- htmlmin:ignore -->
<div class="columns">
  <div class="column pad-small-all">
      <figure>
        {% include './assets/image/svg-x-ray-fx/selfie-with-skull-no-mask.svg' %}
        <figcaption>Skin and bone layers, no clip-path</figcaption>
      </figure>
  </div>
    
  <div class="column pad-small-all">
    <figure>
      {% include './assets/image/svg-x-ray-fx/selfie-with-skull-masked.svg' %}
      <figcaption>Clip-path applied to bone layer</figcaption>
    </figure>
  </div>
</div>

<h2>Controlling the X-ray effect</h2>
<p>The mask is driven by user interaction, so it&rsquo;s time to leverage some 
browser-native API goodness with a small amount of JS. Here&rsquo;s what needs to
happen:</p>

<ul>
  <li>the screen coordinates of the input device will be captured</li>
  <li>these coordinates will be translated to the SVG&rsquo;s coordinate system</li>
  <li>the mask position will be updated as the input device moves</li>
</ul>

<h3>Set up helpful declarations</h3>
<!-- htmlmin:ignore -->
<div class="code-block">
const selfie = document.querySelector( '.surface' );
const mask = document.getElementById( '.mask-shape' );
const coords = selfie.createSVGPoint();
</div>
<!-- htmlmin:ignore -->
<p><code>createSVGPoint()</code> sets up an object with properties describing a point
in an SVG&rsquo;s coordinate system. This will be helpful when translating the 
screen coordinate context to the SVG&rsquo;s own system.</p>

<h3>Capture and manage input coordinates</h3>
<!-- htmlmin:ignore -->
<div class="code-block">
document.addEventListener(
  'mousemove',
  e => {
    setCoords( getCoords( e, selfie ) );
  },
  false
);

let getCoords = (e, svg) => {
  coords.x = e.clientX;
  coords.y = e.clientY;

  return coords.matrixTransform( svg.getScreenCTM().inverse() );
}
</div>
<!-- htmlmin:ignore -->
<p>Ultimately, <code>getCoords</code> returns a set of 2D coordinates that will
visually sync the input device position with the SVG&rsquo;s own coordinate system
(i.e. the mask will align perfectly to the mouse pointer location). For conciseness,
the <code>getCoords</code> function does some double-handling here:</p>

<ul>
  <li>The user&rsquo;s screen coordinates are assigned as property values of the
  <code>coords</code> object. <code>coords</code>, per the declaration above, is
  an <code>SVGPoint()</code> object. Properties of this object relate directly to the SVG&rsquo;s
  own coordinate system.</li>
  <li>In general, the return handles mapping from one coordinate 
  system&mdash;the screen&mdash;to another: the SVG&rsquo;s own coordinate system.
  This part is handled with the <code>getScreenCTM()</code>, which translates to
  the screen context. Chaining <code>inverse()</code> here ensures the mapping occurs in reverse,
  translating to the document context. A <code>matrixTransform()</code> applied to
  the <code>coords</code> object finally returns the desired coordinates.</li>
</ul>

<p>If that doesn&rsquo;t make a whole lot of sense, apologies! There was a fair 
bit of trial and error here for me, and I don&rsquo;t have a good grasp
on this matrix transformation stuff (<a href="https://docs.microsoft.com/en-us/previous-versions/windows/internet-explorer/ie-developer/samples/hh535760(v=vs.85)">the 
math behind this</a> is beyond my feeble brain).</p>

<h3>Update the mask position</h3>

<p>All that needs to be done from here to set the mask location is to update the
<code>cx</code> and <code>cy</code> attributes of the mask element with the freshly
mapped coordinates:</p>
<!-- htmlmin:ignore -->
<div class="code-block">
let setCoords = coordinates => {
  scope.setAttribute( 'cx', coordinates.x );
  scope.setAttribute( 'cy', coordinates.y );
}
</div>
<!-- htmlmin:ignore -->
<p>The mask is now synced to the input device location, like this:</p>

{% include './assets/image/svg-x-ray-fx/skinxbone.svg' %}
<script>"use strict";var bodyLayers=document.querySelector(".js-layers");var testMask=document.querySelector(".js-mask-shape");var coords=bodyLayers.createSVGPoint();window.addEventListener("mousemove",function(e){testSetCoords(testGetCoords(e,bodyLayers))},!1);var testGetCoords=function testGetCoords(e,svg){coords.x=e.clientX;coords.y=e.clientY;return coords.matrixTransform(svg.getScreenCTM().inverse())};var testSetCoords=function testSetCoords(coordinates){testMask.setAttribute("cx",coordinates.x);testMask.setAttribute("cy",coordinates.y)}</script>

<h2>Putting it all together</h2>
<!-- htmlmin:ignore -->
<div class="code-block">
&lt;svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 350 500"&gt;
  &lt;defs&gt;
    &lt;clipPath id="mask"&gt;
      &lt;circle class="mask-shape" cx="0" cy="0" r="20%"/&gt;
    &lt;/clipPath&gt;
  &lt;/defs&gt;
  &lt;g class="surface"&gt;
    &lt;path... /&gt;
  &lt;/g&gt;
  &lt;g clip-path="url(#mask)" class="structure"&gt;
    &lt;path...
  &lt;/g&gt;
&lt;/svg&gt;

&lt;script&gt;
  const selfie = document.querySelector( '.surface' );
  const mask = document.querySelector( '.mask-shape' );
  const coords = selfie.createSVGPoint();

  document.addEventListener(
    'mousemove',
    e => {
      setCoords( getCoords( e, selfie ) );
    },
    false
  );

  let getCoords = ( e, svg ) => {
    coords.x = e.clientX;
    coords.y = e.clientY;

    return coords.matrixTransform( svg.getScreenCTM().inverse() );
  }

  let setCoords = coordinates => {
    mask.setAttribute( 'cx', coordinates.x );
    mask.setAttribute( 'cy', coordinates.y );
  }
&lt;/script&gt;
</div>
<!-- htmlmin:ignore -->
<div class="is-scheme-pink pad-big-y">
  <div class="x-ray" id="js-x-ray" style="min-height: 500px;">
    <figure>
      {% include './assets/icons/selfie.svg' %}
      {% include './assets/icons/selfie-skull.svg' %}
    </figure>
  </div>
</div>