---
layout: templates/post.html
title: Precision layout with the Columns block
date: 2020-11-13
lastModified: 2021-07-10
tags: ['note']
type: wordpress
permalink: "better-columns-block-editor/"
metaDescription: How to set up custom predefined layouts for the Columns block.
hasBreadcrumb: true
wasUpdated: true
---
<p>Predefined layouts in the Columns block have been around for a long time now,
first shipped in the Gutenberg plugin back in June 2019. This genuinely helpful
feature is a prime example of how the block editor can improve user experience
when composing content in a CMS like WordPress: potentially dull and technically
challenging decision-making for the user is solved by a visual prompt and a single click.
Unfortunately, some of these layouts are not precisely defined for the front-end.</p>

<p>Here, I’ll be demonstrating a solution to achieve more intentional column layout patterns, 
using the <a href="https://make.wordpress.org/core/2020/02/27/introduce-block-variations-api/">Block Variations API</a>
available since WordPress 5.4. This approach will allow you to add predefined patterns
to the Columns block, specific to your project requirements.</p>

<h2>The Columns block layout picker</h2>

<p>The default patterns offered for the Columns block are common, highly reusable
container types for page content. Three of the current offered options are layouts
with irregular column widths:

<ul>
  <li><b>70/30</b></li>
  <li><b>30/70</b></li>
  <li><b>25/50/25</b></li>
</ul>

<p>These labels roughly correspond to container ratios of 2:1, 1:2, and 1:2:1, respectively.</p>

<p>Building a layout system to account for multiple possible configurations of sibling
and nested content is prone to alignment errors, even when taking
<a href="https://every-layout.dev/">a defensive approach to CSS</a>. If you’re
anything like me, you appreciate having a reference grid as an aid to visually reveal
errors that can easily slip into a build.</p>

<p>For example, choosing the <code>70/30</code> pattern results in a column layout
that feels off when compared against a reference grid:</p>

<figure>
  <picture>
    <source srcset="{{ site.url }}/assets/image/columns-block-default-pattern.webp">
    <img src="{{ site.url }}/assets/image/columns-block-default-pattern.jpg"
      height="1260"
      width="313"
      alt=""
      loading="lazy"
    />
  </picture>
  <figcaption>The native 70/30 pattern</figcaption>
</figure>

<h2>Primary pain points to address</h2>

<ul>
  <li>Layout pattern has inline styles applied</li>
  <li>Block stylesheet applies an arbitrary margin</li>
  <li>Pattern label is not representative of actual outcome</li>
</ul>

<h3>Layout pattern has inline styles applied</h3>
<!-- htmlmin:ignore -->
<div class="code-block">
&lt;div class="wp-block-columns"&gt;
  &lt;div class="wp-block-column"
    style="flex-basis:66.66%"&gt;
  &lt;/div&gt;
  &lt;div class="wp-block-column"
    style="flex-basis:33.33%"&gt;
  &lt;/div&gt;
&lt;/div&gt;
</div>
<!-- htmlmin:ignore -->
<p>While the label says 70/30, the style declarations <code>flex-basis: 66.66%</code>
and <code>flex-basis: 33.33%</code> are automatically applied inline to the wide
and slim columns, respectively.</p>

<h3>Block stylesheet applies an arbitrary margin</h3>
<!-- htmlmin:ignore -->
<div class="code-block">
.wp-block-column:not(:first-child) {
  margin-left: 32px;
}
</div>
<!-- htmlmin:ignore -->
<p><code>margin-left: 32px</code> is applied to each <code>.wp-block-column</code>, 
excluding the first instance. This presumes you’re not already opting to dequeue
the stylesheet shipped with the block editor for front-end display.</p>

<h3>Pattern label is not descriptive of end result</h3>
<p>70/30 simply doesn’t match the actual values applied to each column width.</p>

<h2>The goal</h2>

<p>Ultimately, the columns are not aligned to the visual grid I’m using. Here’s
the ideal outcome in this instance:</p>

<figure>
  <picture>
    <source srcset="{{ site.url }}/assets/image/columns-block-custom-pattern.webp">
    <img src="{{ site.url }}/assets/image/columns-block-custom-pattern.jpg"
      height="1260"
      width="313"
      alt=""
      loading="lazy"
    />
  </picture>
  <figcaption>A Columns block using a custom 2:1 pattern aligns perfectly to this project’s grid.</figcaption>
</figure>

<h2>Setting up a custom columns variation</h2>

<h3>Unregister the default pattern</h3>
<p>First, let’s unregister the default 70/30 pattern before adding our own custom one:</p>

<!-- htmlmin:ignore -->
<div class="code-block">
  /* Unregister native pattern. */

  wp.blocks.unregisterBlockVariation(
    'core/columns',
    'two-columns-two-thirds-one-third'
  );
</div>
<!-- htmlmin:ignore -->
<p>A simple way to find a block variation’s registered name (and an excuse to go
digging in the latest version of the block editor) is in the Gutenberg plugin
repository. For this example, the name can be <a href="https://github.com/WordPress/gutenberg">sourced from</a>
gutenberg/packages/block-library/src/columns/variations.js.</p>

<h3>Register a custom pattern</h3>

<p>It’s a case of simple configuration from here:
<!-- htmlmin:ignore -->
<div class="code-block">
  /* Register custom pattern. */

  wp.blocks.registerBlockVariation( 'core/columns', {
    name: 'chthonic-columns-two-third-one-third',
    title: '66 / 33',
    icon: iconColumnsTwoThirdOneThird,
    innerBlocks: [
      [ 'core/column', { className: 'is-two-third' } ],
      [ 'core/column', { className: 'is-one-third' } ],
    ],
    scope: [ 'block' ],
  });
</div>
<!-- htmlmin:ignore -->
<p><b>A breakdown of what’s happening here:</b></p>

<ul>
  <li>The Block Variations API is called via the function <code>registerBlockVariation</code>.</li>
  <li>Two parameters are required: the name of the block the variation applies
  to&mdash;<code>core/columns</code>&mdash;followed by a settings object</li>
  <li><b>name</b> is a unique name for the variation</li>
  <li><b>title</b> is the label displayed in the editor UI</li>
  <li><b>icon</b> is a graphic representative of the variation displayed in the editor UI</li>
  <li><b>innerBlocks</b> defines the composition of the variation</li>
  <li>A <b>scope</b> of '<code>block</code>' ensures the variation only appears in the Columns block layout picker</li>
</ul>

<p>A unique CSS class has also been added to each column via the settings object
for each <code>core/column</code> block used.</p>

<h3>Set up a custom icon</h3>

<p>In this example I’m creating a variation with a suitable icon already available
but this won’t always be the case. Adding a custom icon can be achieved as follows:</p>
<!-- htmlmin:ignore -->
<div class="code-block">
  /* Set up custom icon. */

  const el = wp.element.createElement;
  const SVG = wp.primitives.SVG;

  const iconColumnsTwoThirdOneThird = el(
    SVG,
    { width: 48, height: 48, viewBox: '0 0 48 48' },
    el('path', {
      fillRule: 'evenodd',
      clipRule: 'evenodd',
      d:
      'M39 12C40.1046 12 41 12.8954 41 14V34C41 35.1046 40.1046 36 39 36H9C7.89543 36 7 35.1046 7 34V14C7 12.8954 7.89543 12 9 12H39ZM39 34V14H30V34H39ZM28 34H9V14H28V34Z',
    })
  );
</div>
<!-- htmlmin:ignore -->
<h2>A new block variation</h2>

<p>That’s it! The resulting markup for this example looks like this:</p>
<!-- htmlmin:ignore -->
<div class="code-block">
&lt;div class="wp-block-columns"&gt;
  &lt;div class="wp-block-column is-two-third"&gt;
  &lt;/div&gt;
  &lt;div class="wp-block-column is-one-third"&gt;
  &lt;/div&gt;
&lt;/div&gt;
</div>
<!-- htmlmin:ignore -->
<p>At this point the block variation appears as an option in the Columns block layout
picker and is ready to receive CSS specific to your project.</p>

<h3>All together now</h3>
<!-- htmlmin:ignore -->
<div class="code-block">
window.addEventListener('load', function () {

  /* Unregister native pattern. */
  wp.blocks.unregisterBlockVariation(
    'core/columns',
    'two-columns-two-thirds-one-third'
  );

  /* Set up icon. */
  const el = wp.element.createElement;
  const SVG = wp.primitives.SVG;

  const iconColumnsTwoThirdOneThird = el(
    SVG,
    { width: 48, height: 48, viewBox: '0 0 48 48' },
    el('path', {
      fillRule: 'evenodd',
      clipRule: 'evenodd',
      d:
      'M39 12C40.1046 12 41 12.8954 41 14V34C41 35.1046 40.1046 36 39 36H9C7.89543 36 7 35.1046 7 34V14C7 12.8954 7.89543 12 9 12H39ZM39 34V14H30V34H39ZM28 34H9V14H28V34Z',
    })
  );

  /* Register custom pattern. */
  wp.blocks.registerBlockVariation( 'core/columns', {
    name: 'chthonic-columns-two-third-one-third',
    title: '66 / 33',
    icon: iconColumnsTwoThirdOneThird,
    innerBlocks: [
      [ 'core/column', { className: 'is-two-third' } ],
      [ 'core/column', { className: 'is-one-third' } ],
    ],
    scope: [ 'block' ],
  });
});
</div>
<!-- htmlmin:ignore -->

<h3>Avoiding wp.domReady()</h3>
<p><code>wp.domReady</code> appears to have been broken since the end of 2020. The 
above snippet has been updated to use <code>window.addEventListener('load', function () {}</code>.</p>


<h2>Some context for doing this</h2>
<p>It’s important to stress this isn’t some kind of futile attempt to achieve
pixel perfection. This is purely about ensuring predictable and intentional container
structures.</p>

<p>The issue described here is also likely not considered to be an issue at all
by anyone working from a CMS-first perspective. Following along with the Gutenberg
project, it’s often felt to me as though conversations there consider all content
design to be solved prior to actual content being involved. This perspective may 
help with understanding how some of the more opinionated presentation rules have made
their way into the stylesheet shipped with the block editor.</p>