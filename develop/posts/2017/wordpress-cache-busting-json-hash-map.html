---
layout: templates/post.html
title: How to create and retrieve cache-busting filenames for static assets in WordPress
date: 2017-09-27
lastModified: 2017-09-27
tags: ['note']
type: wordpress
permalink: 'wordpress-cache-busting-json-hash-map/'
metaDescription: A quick and easy approach to cache busting filenames in WordPress using a JSON hash map and a small PHP function.
hasBreadcrumb: true
---
<p>Admission: I&rsquo;ve never had a workflow in place for developing WordPress websites where static assets can be set to expire in the far future without the worry of how to serve an updated version.</p>

<p>I would guess and cherry-pick assets with a high probability of remaining unchanged, telling browsers to assume they would never expire, while manually versioning &ldquo;likely to change&rdquo; files with a query string (and, yup, I&rsquo;m aware of the <a href="http://www.stevesouders.com/blog/2008/08/23/revving-filenames-dont-use-querystring/">ancient 2008 prophecy</a> with regards to URLs using query strings).</p>

<p>In actuality, this hasn&rsquo;t been as horrific as it sounds, largely due to the type of sites I tend to end up working on: highly-customised WordPress websites that are treated as set-and-forget by clients as soon as they&rsquo;re launched. Regardless: so sorry.</p>

<p>But! I recently worked on a new site using <a href="https://gohugo.io/">Hugo</a>, treating it as an opportunity to quash some bad practices. Of course, someone had already solved and typed up their <a href="http://danbahrami.io/articles/building-a-production-website-with-hugo-and-gulp-js/">sweet solution for cache-busting static assets</a> for Hugo. This got me thinking about how to implement something similar in WordPress.</p>

<p>To keep things simple, let&rsquo;s deal with a single asset: the WordPress theme&rsquo;s stylesheet. </p>

<h2>The WordPress way</h2>

<p>Every WordPress theme expects a file named <code>style.css</code>, and it&rsquo;s one of the few files that cannot be omitted from a theme directory. This file performs two tasks:</p>

<ul>
    <li>It serves as a known location within WordPress for the theme&rsquo;s stylesheet.</li>
    <li>It contains a header which is used to provide useful details about the theme: name, author, version; and so on.</li>
</ul>

<p>This means any theme directory serving an alternate file containing the theme&rsquo;s style rules should still contain a <code>style.css</code> file. As you&rsquo;ll see in the PHP function below, the default <code>style.css</code> will serve as a fallback in case of a missing or mis-matched hashed filename.</p>

<h2>Creating a hashed filename</h2>

<p>This part is really down to what you&rsquo;re using for tooling: Grunt;  gulp; webpack; that new one everyone is using that I&rsquo;ll find out about in a few months&rsquo; time.</p>

<p>For example, the following webpack config generates a CSS file called <code>style.[contenthash].min.css</code> to <code>dist/css/</code>, where [contenthash] is a unique string. A JSON hash map in <code>/dist</code> will be used by the PHP function below to map the correct hashed filename to style.css.</p>
<!-- htmlmin:ignore -->
<div class="code-block">
const path = require('path');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const WebpackAssetsManifest = require('webpack-assets-manifest');

module.exports = {
    entry: './src/sass/index.js',
    output: {
        style: path.resolve(__dirname, 'dist')
    },
    plugins: [
        new MiniCssExtractPlugin({
            filename: 'css/[name].[contenthash].css'
        }),
        new WebpackAssetsManifest()
    ],
    module: {
        rules: [
            {
                test: /\.scss$/,
                use: [
                    MiniCssExtractPlugin.loader,
                    'css-loader',
                    'sass-loader'
                ]
            }
        ]
    }
}
</div>
<!-- htmlmin:ignore -->
<p>Regardless of what is used, the following needs to be generated:</p>

<ul>
    <li>A version of <code>style.css</code> with a content hash as part of the filename.</li>
    <li>A JSON object with the key as <code>style.css</code> and the value as the hashed filename.</li>
</ul>

<p>Additionally, you&rsquo;ll need new locations for the new files. In the following example I&rsquo;ll use &ldquo;data&rdquo; and &ldquo;dist&rdquo;:</p>

<ul>
    <li><code>data/</code> contains the JSON hash map.</li>
    <li><code>dist/</code> contains the hash-appended files.</li>
</ul>

<h2>Using the JSON hash map</h2>

<p>PHP time! Here&rsquo;s a function to place in functions.php that will perform the work of retrieving a hashed filename, if it exists.</p>
<!-- htmlmin:ignore -->
<div class="code-block">
/**
 * Serve theme styles via a hashed filename instead of WordPress' default style.css.
 *
 * Checks for a hashed filename as a value in a JSON object.
 * If it exists: the hashed filename is enqueued in place of style.css.
 * Fallback: the default style.css will be passed through.
 *
 * @param string $css is WordPress&rsquo; required, known location for CSS: style.css
 */

function get_path_css( $css ) {
    $map = get_template_directory() . '/data/css/hash.json';
    static $hash = null;

    if ( null === $hash ) {
        $hash = file_exists( $map ) ? json_decode( file_get_contents( $map ), true ) : [];
    }

    if ( array_key_exists( $css, $hash ) ) {
        return '/dist/css/' . $hash[ $css ];
    }

    return $css;
}</div>
<!-- htmlmin:ignore -->
<div>

<h2>How it works</h2>

<ul>
    <li>When the function is called a value of <code>style.css</code> is passed in.</li>
    <li>A value for <code>$hash</code> is set if it doesn't exist already (as a static variable its value persists, so the check for a null value ensures the value is only set one time).</li>
    <li>The retrieved JSON hash map is converted to a PHP array; if nothing is retrieved an empty array is set.</li>
    <li><code>array_key_exists()</code> is used to check the existence of the function's passed in value—style.css—as an array key, and, if it exists, the value for this key is returned.</li>
    <li>Otherwise, the default <code>style.css</code> is returned.</li>
</ul>

<h2>Serving the hashed filename</h2>

<p>Calling the function occurs as part of the enqueuing process for style.css. get_path_css() hijacks the second parameter of <a href="https://developer.wordpress.org/reference/functions/wp_enqueue_style/">wp_enqueue_style</a> in order to dynamically determine the correct path.</p>
<!-- htmlmin:ignore -->
<div class="code-block">
/**
 * Enqueue the stylesheet.
 */
wp_enqueue_style(
    'theme-styles',
    get_stylesheet_directory_uri() .
    get_path_css( 'style.css' )
);
</div>
<!-- htmlmin:ignore -->
<h2>Fallback</h2>

<p>If there's a mismatch in filenames or the file simply doesn&rsquo;t exist, the default style.css will be enqueued.</p>
