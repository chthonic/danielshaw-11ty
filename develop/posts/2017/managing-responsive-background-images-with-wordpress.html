---
layout: templates/post.html
title: Managing responsive background images with WordPress
date: 2017-06-01
lastModified: 2017-06-01
tags: ['note']
type: wordpress
permalink: 'managing-responsive-background-images-with-wordpress/'
metaDescription: How to set per-page responsive background images in WordPress without setting hard-coded image paths in a stylesheet.
hasBreadcrumb: true
---
<p>This is a solution allowing for per-page responsive background images without setting hard-coded image paths in a stylesheet. This example assumes the image is set as the <a href="https://en.support.wordpress.com/featured-images/">featured image</a> for a page.</p>

<p>The two display issues we’ll solve here are both context-based: how to avoid serving an overly-heavy image to smaller screens, and how to adapt the image aspect-ratio to better accommodate the user’s current screen width.</p>

<h2>The workhorse image</h2>

<p>I regularly see images destined for the browser asked to perform an impossible task: dimension, format and aspect-ratio are each expected to be completely fluid while maintaining the integrity of the visual content.</p>

<p>A common device is an image with text overlaid—for example, imagine a banner at the top of a page. On a wider screen this will typically be heavily landscape-biased, but what happens when the available screen width is scaled down?</p>

<p>When viewed on a smaller screen the image will, by default, maintain its aspect ratio and be reduced to a short-&amp;-wide strip. In conflict with this, text will flow downwards as line-lengths are considerably constrained by shrinking horizontal space.</p>

<p>This means the height of the image cannot be pre-determined; instead, it will always be determined by the dynamic height of the text block.</p>

<h2>Background-size vs. object-fit</h2>

<p>Choosing whether to use <code>&lt;img&gt;</code> with <code>object-fit</code> or a background image with <code>background-size</code> comes down to how the image needs to perform—<a href="http://caniuse.com/#search=object-fit">browser support for object-fit</a> aside—and layout requirements imposed by the design.</p>

<p><code>background-size</code> and <code>object-fit</code> can be configured to instruct images to seep into all available space while preserving aspect-ratio. Both properties do this at the expense of controlling the visible width and/or height of an image. For more precision in how images are displayed, cropping becomes an ally.</p>

<p>The approach here works well where there's no SEO benefit lost by using a background image, and regardless of whether the image is displaying meaningful visual content or simply to provide "texture".</p>

<h2>Defining alternate images</h2>

<p>First, it’s necessary to tell WordPress to prepare new images for display. All images uploaded in the WordPress admin—via the Add Media button, Add Featured Image link, or directly in the Media section—can be easily targeted for resizing and cropping. In this example the image would be uploaded via the Add Featured Image link.</p>

<p>Let's assume two image sizes are required, <code>large</code> and <code>small</code> versions:</p>
<!-- htmlmin:ignore -->
<div class="code-block">
/*
 * Enable support for Post Thumbnails on posts and pages.
 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
 */
add_theme_support( 'post-thumbnails' );

if ( function_exists( 'add_image_size' ) ) {
    add_image_size( 'banner-l', 1600, 600, true );
    add_image_size( 'banner-s', 500, 500, true );
}
</div>
<!-- htmlmin:ignore -->
<p>The first attribute is the unique name for the image; the second and third attributes are the width and height, respectively; the fourth attribute ensures the image is cropped exactly to the specified width and height (as opposed to resizing to one or the other dimension).</p>

<h2>Source image</h2>

<p>Using the image <code>banner.jpg</code> below as input, two new images will be automatically generated per the white rectangles.</p>

<figure>
    <img
    src="{{ site.url }}/assets/image/choosing-image-sizes-wordpress.jpg"
    alt="The Famous Mayan Bee"
    class="lazyload">
    <figcaption>"<a href="http://www.publicdomainpictures.net/view-image.php?image=90103&picture=the-famous-mayan-bee">The Famous Mayan Bee</a>" is used here under the <a href="https://en.wikipedia.org/wiki/Creative_Commons">Creative Commons CC0 1.0 Universal Public Domain Dedication</a>.</figcaption>
</figure>

<p>WordPress-generated images are automatically named according to this convention: <code>filename-[width]x[height].jpg</code>. For example, the <code>large</code> version will be <code>banner-1600x600.jpg</code>, based on a source image called <code>banner.jpg</code>.</p>

<h2>Retrieving the images</h2>

<p>All we need to know to retrieve each image size is its URI, something <a href="https://developer.wordpress.org/reference/functions/wp_get_attachment_image_src/">wp_get_attachment_image_src()</a> can provide. This example uses a featured image to store the target, so <a href="https://developer.wordpress.org/reference/functions/get_post_thumbnail_id/">get_post_thumbnail_id()</a> is used to retrieve the image ID.</p>

<p>The following function finds each variant of the target image and retrieves a URI. This is used to construct CSS media queries that will be inlined in the head of the page, using <a href="https://developer.wordpress.org/reference/functions/wp_add_inline_style/">wp_add_inline_style()</a>. An important caveat for using <code>wp_add_inline_style()</code> is the first argument must reference a stylesheet that is already loaded:</p>
<!-- htmlmin:ignore -->
<div class="code-block">
/**
* Prints inline CSS to the header for a responsive background banner image
* @link https://developer.wordpress.org/reference/functions/wp_add_inline_style/
*/

function the_theme_css() {
  // wp_add_inline_style must reference an existing stylesheet
  wp_enqueue_style( 'theme-css', get_template_directory_uri() . '/style.css' );

  // Get the featured image
  $featured_id = (int)get_post_thumbnail_id();

  if ( !empty ( $featured_id ) ) {
    // Get the cropped versions of the image
    $banner_s = wp_get_attachment_image_src( $featured_id, 'banner-s' )[0];
    $banner_l = wp_get_attachment_image_src( $featured_id, 'banner-l' )[0];

    // Describe CSS to be set inline
    $page_banner_css = '.banner{background-image:url(' . esc_url ( $banner_s ) . ');}@media(min-width: 75em){.banner{background-image:url(' . esc_url( $banner_l ) . ');}}';

    // Set the CSS inline
    wp_add_inline_style( 'theme-css', $page_banner_css );
  }
}

add_action( 'wp_enqueue_scripts', 'the_theme_css' );
</div>
<!-- htmlmin:ignore -->
<p>&hellip;and as it appears in <code>&lt;head&gt;</code>:</p>
<!-- htmlmin:ignore -->
<div class="code-block">
&lt;style id="theme-css-inline-css"&gt;
.banner{background-image:url(https://example.com/path/to/banner-500x500.jpg);}@media(min-width:43.75em){.banner{background-image:url(https://example.com/path/to/banner-1600x600.jpg);}}
&lt;/style&gt;
</div>
<!-- htmlmin:ignore -->
<h2>Using the images in a theme</h2>

<p>The following markup uses the .banner class to set the background image:</p>
<!-- htmlmin:ignore -->
<div class="code-block">
&lt;style&gt;
.banner {
  background-position: center center;
  background-size: cover;
}

.banner__inner {
  display: flex;
  flex-direction: column;
  justify-content: center;
}
&lt;/style&gt;

&lt;div class="banner"&gt;
  &lt;div class="banner__inner"&gt;
    &lt;h1&gt;The Bee&lt;/h1&gt;
    &lt;p&gt;Studies have shown dramatic declines in bee colonies.&lt;/p&gt;
  &lt;/div>    
&lt;/div>
</div>
<!-- htmlmin:ignore -->
<p>&hellip; and achieves the following output:</p>

<style>.banner{background-position:center center;background-size:cover;color:#fff;max-width:100%}.banner .h1{font-size:2.5em}.banner p{margin-top:.75rem}.banner__inner{background-color:rgba(0,0,0,.5);display:flex;flex-direction:column;height:100%;justify-content:center;padding:0 1.5rem; width:100%;}.banner--large{background-image:url(/assets/image/bg-large-bee.jpg);height:227px;max-width:604px;width:100%;}.banner--small{background-image:url(/assets/image/bg-small-bee.jpg);height:250px;width:250px}</style>

<div class="banner banner--large">
    <div class="banner__inner">
    <span class="h1">The bee</span>
    <p>Studies have shown dramatic declines in bee colonies.</p>
    </div>
</div>

<div class="banner banner--small">
    <div class="banner__inner">
    <span class="h1">The bee</span>
    <p>Studies have shown dramatic declines in bee colonies.</p>
    </div>
</div>

<h2>Existing images</h2>

<p>Existing images are not automatically resized when new image dimensions are specified via <code>add_image_size()</code>. WordPress needs to be instructed to regenerate this media.</p>

<p>If you're using <a href="http://wp-cli.org/">WP-CLI</a>, it's just a case of running <code>'wp media regenerate'</code>. All new image size definitions will become available as a result. To generate from a single image you can add the image id to the end of that command: <code>'wp media regenerate 10'</code> would only apply to the uploaded image with an ID of 10.</p>

<p>If WP-CLI isn't an option, there are a number of plugins devoted to this task.</p>

<h2>Choosing breakpoints for images</h2>

<p>Natural breakpoints for textual content will usually be suggested by the content itself: when the layout begins to look unintentional, a new breakpoint needs to be set. However, choosing image breakpoints can require a bit more thought. In this case it's wise to consider both the <b>file size</b> and whether the image remains <b>visually meaningful</b> (a bad crop or strange alignment will turn a 'content' image into a 'texture' image).</p>
