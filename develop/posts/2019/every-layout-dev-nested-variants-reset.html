---
layout: templates/post.html
title: Every Layout is a salve
date: 2019-06-19
lastModified: 2019-06-19
tags: ['note']
type: css
permalink: 'every-layout-dev-nested-variants-reset/'
metaDescription: It’s rare to find writing around front-end development that is genuinely insightful and immediately useful.
hasBreadcrumb: true
---
<p>There are a few people I return to often for their clarity of 
intent and empathy for the long-suffering end user. Each of these people have 
influenced how I prepare content for a web browser: 
<a href="https://v6.robweychert.com/">Rob Weychert</a> for amazing design-led
code exploration; <a href="https://www.zachleat.com/">
Zach Leatherman</a> for font research; <a href="http://www.heydonworks.com/">
Heydon Pickering</a> for his thinking on accessibility and general sensibility.</p>

<p>That last guy<sup><a href="#note-1">1</a></sup> has easily had the most enduring
impact on how I manage presentation with CSS, via a stupidly simple—the best kind of stupid—but powerful idea: the 
<a href="https://alistapart.com/article/axiomatic-css-and-lobotomized-owls/">
lobotomized owl</a>. He's recently launched <a href="https://every-layout.dev/">
Every Layout</a>, a resource to help &ldquo;relearn CSS layout&rdquo;. There's only 
a few topics available to read right now, but what I've seen so far 
serves as a timely antidote to discussions around CSS that have been painful to 
follow in recent years.</p>

<h2>CSS is not &ldquo;broken&rdquo;</h2>

<p>Or, at least, it's only as broken as every single system in
existence when viewed through a frame of either original sin or a future 
utopia. CSS's global state does not need to be 
an issue when structured with an appreciation for 
<a href="https://www.google.com/search?q=cascading">the big C</a>. When the 
majority of the URLs on the web still resolve as websites that embrace the 
static-document-as-a-web-page model, it seems wise to understand and work with 
the available tool instead of developing all new tools with all new problems. 
Very few people are actually building rockets and rocket science does not need
to be applied to this situation.</p>

<p>&ldquo;<a href="https://every-layout.dev/layouts/stack/">The Stack</a>&rdquo;</a> 
discusses the <code>* + *</code> selector, or, how to manage elements'
relationships with each other in a document's vertical flow. This really 
hits on one of the main tasks when developing for the front-end: taking a set of components 
and describing rules to govern those components, irrespective of their ultimate 
relationships not being known. It's no Skynet, but there is a level of 
faux self-awareness being created here. What really delights and amazes is 
how cleanly this approach solves two quite different use-cases: developer-controlled 
templates and user-generated content managed via CMS.</p>

<p>Back to the Stack: “the trick is to style the context, not the 
individual element(s). The Stack layout primitive injects margin between 
elements via their common parent”:</p>
<!-- htmlmin:ignore -->
<div class="code-block">
.stack > * + * {
  margin-top: 1.5rem;
}
</div>
<!-- htmlmin:ignore -->
<p>This is a best friend of intentionality! Gain confidence and influence as you exert uniform control 
on the vertical flow of content!</p>

<h2>Managing nested variants</h2>

<p>It's rare all elements can be managed with a single margin value. The Stack approach 
addresses this with the suggestion to &ldquo;set up alternative non-recursive Stacks 
with different margin values, and nest them where suitable.&rdquo; The suggested 
approach looks like this:</p>
<!-- htmlmin:ignore -->
<div class="code-block">
[class^='stack'] > * {
  margin: 0;
}

.stack--small > * + * {
  margin-top: 1.5rem;
}

.stack--large > * + * {
  margin-top: 3rem;
}
</div>
<!-- htmlmin:ignore -->
<p>My experience has been it's not too helpful to use alternative wrapper classes 
to manage variations, and I choose to assign more explicit over-rides:</p>
<!-- htmlmin:ignore -->
<div class="code-block">
/* No wrapper class available. */
.stack h2,
.stack h3 {
  margin-top: 3rem;
}

/* Wrapper class inserted by a 3rd-party. */
.stack .wp-block-quote {
  margin-top: 3rem;
}
</div>
<!-- htmlmin:ignore -->
<p>This ignores the context and targets the element, which—OMG—goes against the entire concept of the Stack. 
In practice, I've found this to be a happy trade-off for elements where a wrapper 
class cannot be controlled (user-generated content via a CMS) or a known wrapper class will be
injected by a 3rd-party (say, the &ldquo;block&rdquo; divs WordPress now wraps 
around some elements).</p>

<h2>Hmmm, Firefox&hellip;</h2>

<p>The following markup displays oddly in Firefox when the <code>* + *</code> 
selector is used recursively:</p>
<!-- htmlmin:ignore -->
<div class="code-block">
&lt;style&gt;
  .stack * + * {
    margin-top: 1.5rem;
  }
&lt;/style&gt;

&lt;div class="stack"&gt;
  &lt;p&gt;c/o Cecil &amp; Minerva&lt;br&gt;
  Owhiro Bay&lt;br&gt;
  Wellington 6023&lt;br&gt;
  New Zealand&lt;/p&gt;
&lt;/div&gt;
</div>
<!-- htmlmin:ignore -->
<p>In Firefox this results in a margin being applied to the <code>&lt;br&gt;</code> element: see a <a href="https://codepen.io/chthonic/pen/LKRoQP">demonstration of this issue</a> on Codepen (must be viewed in Firefox).</p>

<p>When first using this selector, I noticed random paragraphs with forced 
line-breaks were being spaced oddly in Firefox: some lines were displaying 
with an upper margin applied, generating unintentional white-space. Eventually 
I realised the <code>&lt;br&gt;</code> element was also inheriting the margin.</p>

<p>The solution is simple:</p>
<!-- htmlmin:ignore -->
<div class="code-block">
/* Prevent margin being added to &lt;br&gt; in Firefox. */
.stack br {
  margin-top: 0;
}
</div>
<!-- htmlmin:ignore -->
<div class="footnotes">
  <ol>
    <li id="note-1">Yup, I acknowledge this list is Dude Fest 2019.</li>
  </ol>
</div>
