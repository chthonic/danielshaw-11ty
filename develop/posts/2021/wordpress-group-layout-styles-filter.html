---
layout: templates/post.html
title: "How to filter styles for WordPress blocks with layout support"
date: 2021-07-07
lastModified: 2022-01-31
tags: ['note']
type: WordPress
permalink: "wordpress-block-layout-customise-inline-styles/"
metaDescription: "A solution for customising a layout-supported Group block's style element, to allow user-defined values to drive a different layout model."
hasBreadcrumb: true
wasUpdated: true
---

<p>I spent much of June working on a forward-looking WordPress theme, in anticipation of 
&ldquo;<a href="https://wptavern.com/what-is-full-site-editing-and-what-does-it-mean-for-the-future-of-wordpress" rel="nofollow">Full 
Site Editing</a>&rdquo; arriving in WordPress 5.9 at the end of 2021.<sup><a id="note-1-ref" href="#note-1" aria-describedby="footnote-label">1</a></sup> 
There was a small snag early on with the CSS syntax delivered inline alongside the new layout-supported 
Group block: it presumes one type of layout model by default.</p>
	
<p>What follows is a solution for customising the contents of a Group block&rsquo;s <code>style</code> 
element, to match the requirements of a different layout model.</p>

<h2>Default styles for blocks with layout support</h2>
<p>First, what does &ldquo;layout support&rdquo; mean? The settings panel for a 
Group block in WordPress 5.8+ offers a configurable Layout option, allowing 
user-defined widths to be set from inside the block editor.</p>

<p>For dynamic width values to work they must be scoped to each block. WordPress 
handles this by inlining a style element, with styles scoped to a unique auto-generated 
class per block. Unfortunately, these styles presume a layout model using <code>max-width</code> 
paired with <code>margin-left: auto; margin-right: auto</code>. Child elements are 
also presumed to use CSS floats for horizontal alignment.</p>

<p>For example:</p>
<!-- htmlmin:ignore -->
<div class="code-block">
/* Default Group block layout inline styles. */

&lt;style&gt;
	.wp-container-60e5046dbe359 > * { max-width: 100rem; margin-left: auto !important; margin-right: auto !important; }
	.wp-container-60e5046dbe359 > .alignwide { max-width: 100rem; }
	.wp-container-60e5046dbe359 .alignfull { max-width: none; }
	.wp-container-60e5046dbe359 .alignleft { float: left; margin-right: 2em; }
	.wp-container-60e5046dbe359 .alignright { float: right; margin-left: 2em; }
&lt;/style&gt;

</div>
<!-- htmlmin:ignore -->
<p>How to manage this style element for a layout model requiring different CSS syntax?</p>

<h2>Filtering the layout style element</h2>
<p>The layout support styles are printed by a function found at wp-includes/block-supports/layout.php: 
<code>wp_render_layout_support_flag()</code>. 
Touching this directly is not an option, but the good news here is there&rsquo;s a 
<a href="https://developer.wordpress.org/plugins/hooks/filters/" rel="nofollow">filter</a> 
that makes it easy to take over and print a tailored <code>style</code> element 
via a custom function.</p>
<!-- htmlmin:ignore -->
<div class="code-block">
add_filter( 'render_block', 'name_of_custom_function', 10, 2 );
</div>
<!-- htmlmin:ignore -->
<h3>Adding custom CSS</h3>
<p>Working with a copy of the original function, the key part is assigning new CSS declarations to the <code>style</code> variable. 
<a href="https://bitbucket.org/chthonic/workspace/snippets/xXAgR9">This 
snippet</a> <sup><a id="note-2-ref" href="#note-2" aria-describedby="footnote-label">2</a></sup> 
demonstrates how I&rsquo;m returning styles for a layout model based on CSS Grid 
syntax, with the resulting <code>style</code> element returned like this:</p>
<!-- htmlmin:ignore -->
<div class="code-block">
/* Custom Group block layout inline styles. */

&lt;style&gt;
	.wp-container-60e51fc2dbfd2 { max-width:100rem; justify-self:center }
	.wp-container-60e51fc2dbfd2.alignwide { max-width:100rem; }
	.wp-container-60e51fc2dbfd2.alignfull{ max-width:none; }
&lt;/style&gt;
</div>
<!-- htmlmin:ignore -->

<h3>Unhooking native functions</h3>
<p>To prevent multiple style elements being loaded per block, it&rsquo;s necessary 
to run <code>remove_filter</code> for the native core function</code>. The Gutenberg 
plugin currently (as of v10.9.1) has its own implementation which should also be unhooked.</p>

<!-- htmlmin:ignore -->
<div class="code-block">
	remove_filter( 'render_block', 'wp_render_layout_support_flag', 10, 2 );
	remove_filter( 'render_block', 'gutenberg_render_layout_support_flag', 10, 2 );
</div>
<!-- htmlmin:ignore -->
<div class="footnotes">
	<h2 id="footnote-label" class="visually-hidden">Footnotes</h2>
  
	<ol>
	  <li id="note-1">The final name for this functionality seems yet to be resolved, 
	  and it&rsquo;s sadly unclear if this will actually land by the end of the year. <a href="#note-1-ref" aria-label="Back to content">↩</a></li>
	  <li>Originally written for WordPress 5.8, here&rsquo;s an <a href="https://bitbucket.org/chthonic/workspace/snippets/gBGq9g">updated 
		snippet for WordPress 5.9</a> with layout support for additional blocks. <a href="#note-2-ref" aria-label="Back to content">↩</a></li></li>
	</ol>
</div>