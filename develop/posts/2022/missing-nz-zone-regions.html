---
layout: templates/post.html
title: "Fixing New Zealand zone regions in WooCommerce 7.1.0 update"
date: 2022-11-11
lastModified: 2023-01-01
tags: ['note']
type: WooCommerce
permalink: "fixing-nz-zone-regions/"
metaDescription: "New Zealand zone regions may no longer display after updating to WooCommerce 7.1.0."
hasBreadcrumb: true
wasUpdated: true
---

<p>Please note: this issue was partly fixed in the WooCommerce <a href="https://github.com/woocommerce/woocommerce/releases/tag/7.2.1" rel="nofollow">7.2.1 
release</a>, and completely resolved in the <a href="https://github.com/woocommerce/woocommerce/releases/tag/7.2.2" rel="nofollow">7.2.2 release</a>. 
The old zone region IDs will be automatically updated to the new format by clicking 
the &ldquo;Update WooCommerce database&rdquo; link that appears in your dashboard 
after updating the plugin.</p>

<ul>
	<li><a href="#what-changed">What changed</a></li>
	<li><a href="#what-broke">What broke</a></li>
	<li><a href="#why-it-broke">Why it broke</a></li>
	<li><a href="#how-to-fix">How to fix</a></li>
</ul>

<h2 id="what-changed">What changed</h2>

<p>WooCommerce 7.1.0 includes a change to how the "subdivision" codes for matching 
New Zealand regions are formatted. Section <a href="https://raw.githubusercontent.com/woocommerce/woocommerce/trunk/changelog.txt" rel="nofollow">7.1.0 2022-11-08</a> 
of the changelog contains the following item:</p>

<!-- htmlmin:ignore -->
<div class="code-block">
	* Tweak - Update subdivision codes for New Zealand, to match current CLDR specification.
</div>
<!-- htmlmin:ignore -->

<h2 id="what-broke">What broke</h2>

<p>Before updating, regions in the below image are visibly assigned to a “North Island” zone name:</p>

<figure style="max-width: 100%">
	<picture>
	  <source srcset="{{ site.url }}/assets/image/woocommerce-7-10-1-zone-regions-before-upgrade.webp">
	  <img src="{{ site.url }}/assets/image/woocommerce-7-10-1-zone-regions-before-upgrade.jpg"
		height="410"
		width="1260"
		alt="Screenshot of zone regions configured prior to updating to WooCommerce 7.1.0"
		loading="lazy"
	  />
	</picture>
  
	<figcaption>Zone regions configured prior to updating to WooCommerce 7.1.0</figcaption
</figure>

<p>&hellip;but <a href="https://github.com/woocommerce/woocommerce/issues/35535">zone regions are no longer assigned</a> after updating to WooCommerce 7.1.0.</p>

<figure style="max-width: 100%">
	<picture>
	  <source srcset="{{ site.url }}/assets/image/woocommerce-7-10-1-zone-regions-after-upgrade.webp">
	  <img src="{{ site.url }}/assets/image/woocommerce-7-10-1-zone-regions-after-upgrade.jpg"
		height="410"
		width="1260"
		alt="Screenshot of zone regions missing after updating to WooCommerce 7.1.0"
		loading="lazy"
	  />
	</picture>
  
	<figcaption>Zone regions missing after updating to WooCommerce 7.1.0</figcaption
</figure>

<h2 id="why-it-broke">Why it broke</h2>

<p>2&ndash;character IDs were previously used internally by WooCommerce to match 
New Zealand regions. This update introduces a new 3&ndash;character format without 
accounting for saved configurations using the old format.</p>

<p>For example, the zone region “Wellington” used to be assigned a code of <code>WE</code>. 
The recent WooCommerce update now assigns a code of <code>WGN</code> to this region.</p>

<p>This results in a mismatch of IDs where zone regions were assigned to a shipping 
zone prior to updating to WooCommerce 7.1.0. WooCommerce now attempts to match regions 
to the new codes, while previously-configured shipping zones continue to use the 
old codes.</p>

<h2 id="how-to-fix">How to fix</h2>

<p>Here’s a couple of manual quick-fix options until WooCommerce (hopefully) adopts 
an automated solution:</p>

<h3>Reassign regions</h3>

<p>The most straightforward approach is to reassign the correct regions to each 
shipping zone manually. To do this, open each affected zone and set regions anew 
in the “Zone regions” field.</p>

<p>While doing this, it’s worth checking if the Country/State field under Store 
Address (see the General tab) also needs to be fixed.</p>

<h3>Update region IDs</h3>

<p>Another option is to update the database with the new codes. In the <code>wp_woocommerce_shipping_zone_locations</code> 
table, look for the <code>location_code</code> column containing the old format codes.</p>

<p>Here’s a before/after list of codes:</p>

<ul>
	<li><b>Northland.</b> <code>NL</code> becomes <code>NTL</code></li>
	<li><b>Auckland.</b> <code>AK</code> becomes <code>AUK</code></li>
	<li><b>Waikato.</b> <code>WA</code> becomes <code>WKO</code></li>
	<li><b>Bay of Plenty.</b> <code>BP</code> becomes <code>BOP</code></li>
	<li><b>Taranaki.</b> <code>TK</code> becomes <code>TKI</code></li>
	<li><b>Gisborne.</b> <code>GI</code> becomes <code>GIS</code></li>
	<li><b>Hawke’s Bay.</b> <code>HB</code> becomes <code>HKB</code></li>
	<li><b>Manawatu-Wanganui.</b> <code>MW</code> becomes <code>MWT</code></li>
	<li><b>Wellington.</b> <code>WE</code> becomes <code>WGN</code></li>
	<li><b>Nelson.</b> <code>NS</code> becomes <code>NSN</code></li>
	<li><b>Malborough.</b> <code>MB</code> becomes <code>MBH</code></li>
	<li><b>Tasman.</b> <code>TM</code> becomes <code>TAS</code></li>
	<li><b>West Coast.</b> <code>WC</code> becomes <code>WTC</code></li>
	<li><b>Canterbury.</b> <code>CT</code> becomes <code>CAN</code></li>
	<li><b>Otago.</b> <code>OT</code> becomes <code>OTA</code></li>
	<li><b>Southland.</b> <code>SL</code> becomes <code>STL</code></li>
</ul>