---
layout: templates/post.html
title: "A block editor theme called Maisie"
date: 2022-03-26
lastModified: 2022-10-31
tags: ['note']
type: WordPress
permalink: "maisie-block-editor-theme/"
metaDescription: "Maisie is an experimental block editor theme, natively supported by WordPress since its 5.9 release."
hasBreadcrumb: true
wasUpdated: true
---

<figure style="max-width: 100%">
	<picture>
	  <source srcset="{{ site.url }}/assets/image/maisie-splash.webp">
	  <img src="{{ site.url }}/assets/image/maisie-splash.jpg"
		height="1107"
		width="1260"
		alt="Screenshot of an example layout for the Maisie theme"
		loading="lazy"
	  />
	</picture>
  
	<figcaption>Cat image by <a href="https://unsplash.com/@madhatterzone">Manja 
	Vitolic</a></figcaption>
</figure>

<h2>Now archived</h2>
<p>Maisie was an experimental "full site editing" theme, natively supported by WordPress 
since its 5.9 release in January 2022. The goal for the theme was to serve as a base to extend rather 
than as an endpoint with a resolved visual design. The theme was never suited to 
a production website and has <a href="https://github.com/chthonic-ds/maisie" rel="nofollow">since been archived</a>.</p>

<h2>Maisie is an experiment</h2>
<p>This is the first theme I&rsquo;ve worked on unprompted by a content brief, because I&rsquo;ve 
always struggled to understand speculative design that isn&rsquo;t informed by real-world 
&ldquo;content&rdquo;<sup><a id="note-1-ref" href="#note-1" aria-describedby="footnote-label">1</a></sup>. Whether CMS- or theme-driven, technology-first 
approaches to composing layouts often result in a hostile user experience. This was 
always going to be a bit of an odd fit for me.</p>

<p>As a result, Maisie went through <b>many</b> iterations: from niche theme with highly 
resolved design, back to a starter theme with a bare minimum of default configuration, 
to where it finally landed: a solid head-start for the type of projects I work on 
but not so much as to require heavy revision per use. An initial plan to contribute 
to the theme directory was sadly dropped due to filtering the core function 
<code>wp_render&shy;_layout_support_flag()</code>, required to adapt per-block 
generated styles to use CSS Grid properties.</p>

<h3>Why do this?</h3>
<p>The question Maisie poses is this: is it feasible to support, long-term, a block 
theme that has opted out of WordPress-provided CSS?<sup><a id="note-2-ref" href="#note-2" aria-describedby="footnote-label">2</a></sup> 
While I genuinely appreciate the standardisation blocks represent, the gentle lock-in 
inherent in the multiverse of &ldquo;classic&rdquo; WordPress set-ups has been replaced 
by an approach to managing styles that sees the entire project slowly creeping towards 
becoming a <a href="https://github.com/WordPress/gutenberg/issues/38694#issuecomment-1036163040">walled garden</a>.</p>

<p>For me, working with the block editor as intended currently feels like a major trade-off 
because the full expressiveness of CSS is lost. There&rsquo;s a world of difference 
between composing a site within the constraints of the block editor and attempting 
to realise a 3rd-party design brief within those same tight constraints.</p>

<p>A simple example from many encountered when working on Maisie: a <code>border</code> 
UI control is natively available for various block elements. Assigning 
a border with a &ldquo;primary&rdquo; colour, style, and width results in the 
following being added to the block&lsquo;s markup:

<ul>
	<li>Class selectors <code>has-border-color</code> and <code>has-primary-border-color</code></li>
	<li>inlined styles for <code>border-style</code> and <code>border-width</code></li>
</ul>

<p>The inlined styles mean the border setting is tightly coupled with the CSS <code>border</code> 
property. There&lsquo;s an immediate problem with this because, depending on the 
design brief&mdash;say, a motion effect on hover&mdash;it may be better or even 
required to use something like <code>box-shadow</code> to achieve the desired outcome. 
This creates a situation where a choice must be made between using the editor-provided <code>border</code> 
option and accepting a crippled design versus using a custom solution and opting 
out of allowing user control, though this is sometimes further complicated by there 
being no provision for hiding a native control if not required.</p>

<h2>Marking a farewell to WordPress, kind of</h2>
<p>At heart, Maisie is an attempt to &ldquo;bend the tool&rdquo;, something 
conventionally considered a bad idea in the software world. I would like to say it&rsquo;s 
been a fun exploratory project but on reflection, despite a few small victories, it&rsquo;s been 
the least enjoyable thing I&rsquo;ve worked on.</p>

<p>There were a couple of goals I had when starting on Maisie: to help understand new 
opportunities and limitations of the block editor, and to finish with a flexible, production-ready 
theme as a starting point for building client projects. Upon completing Maisie&mdash;in 
so far as an initial release now exists&mdash;it&rsquo;s sadly clear 
WordPress is no longer a suitable option for how I approach making websites.</p>

<h3>The key issue</h3>
<p>Modern WordPress is a moving target that's difficult to scope against, so development 
and maintenance costs have soared. This is an important issue for me because the 
majority of projects I&rsquo;ve chosen to work on are those with small budgets which 
may otherwise have not had a chance to be realised.</p>

<p>There have been some promising signs recently, with small but significant shifts 
in direction arising from increased sensitivity by the Gutenberg team towards 
real-world design & development requirements. There&rsquo;s also been an increase in 
<a href="https://github.com/WordPress/gutenberg/issues/38998">visible discussion</a> 
around concerns that have been bubbling away since the new editor launched 
in WordPress 5.0. What ultimately remains troubling for me is that, despite closely 
following the project closely for ~5 years, I still cannot clearly see where 
this is all heading.</p>

<h2>From here</h2>
<p>Maisie will continue to be nurtured as a hobby project and a point of contact with 
the WordPress project. I&rsquo;m curious as to whether a time will arrive where:</p>

<ul>
	<li>the theme breaks irreparably</li>
	<li>the overlap between theme and block editor development aligns enough to 
	feel comfortable building a project using Maisie</li>
</ul>

<p>More broadly, I&rsquo;ll no longer be starting new projects with WordPress, instead 
shifting focus to maintenance and feature work for existing websites.</p>

<div class="footnotes">
	<h2 id="footnote-label" class="visually-hidden">Footnotes</h2>
  
	<ol>
		<li id="note-1">Such a horribly reductive term. <a href="#note-1-ref" aria-label="Back to content">↩</a></li>
		<li id="note-2">This is something I&rsquo;ve already been doing for most 
		of the <strong>custom</strong> client block themes I&rsquo;ve written. It&lsquo;s 
		proven more challenging long-term to manage CSS that dramatically extends 
		core styles than writing custom styles from scratch. <a href="#note-2-ref" aria-label="Back to content">↩</a></li>
	</ol>
  </div>