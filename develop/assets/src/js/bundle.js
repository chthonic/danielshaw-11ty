// Remove .no-js on body.
document.documentElement.classList.remove('no-js');

// Colour mode
const STORAGE_KEY = 'user-color-scheme';
const COLOR_MODE_KEY = '--color-mode';

const buttonToggle = document.querySelector('.js-toggle');
const buttonToggleLabel = document.querySelector('.js-toggle-label');
const buttonToggleNotice = document.querySelector('.js-toggle-notice');

/**
 * Set toggle initial position.
 */
const initialSetting = () => {
	if (localStorage.getItem(STORAGE_KEY)) {
		var mode = localStorage.getItem(STORAGE_KEY);
		
		if ('light' === mode || null === mode) {
			buttonToggle.setAttribute('aria-checked', false);
		} else {
			buttonToggle.setAttribute('aria-checked', true);
		}
	}
}

initialSetting();

/**
 * Pass in a custom prop key and this function will return its
 * computed value. 
 * A reduced version of this: https://andy-bell.design/wrote/get-css-custom-property-value-with-javascript/
 */
const getCSSCustomProp = (propKey) => {
  let response = getComputedStyle(document.documentElement).getPropertyValue(propKey);

  // Tidy up the string if there’s something to work with
  if (response.length) {
    response = response.replace(/\'|"/g, '').trim();
  }

  // Return the string response by default
  return response;
};

/**
 * Takes either a passed settings ('light'|'dark') or grabs that from localStorage.
 * If it can’t find the setting in either, it tries to load the CSS color mode,
 * controlled by the media query
 */
const applySetting = passedSetting => {
  let currentSetting = passedSetting || localStorage.getItem(STORAGE_KEY);
  
  if(currentSetting) {
    document.documentElement.setAttribute('data-user-color-scheme', currentSetting);
    setButtonLabelAndStatus(currentSetting);
  }
  else {
    setButtonLabelAndStatus(getCSSCustomProp(COLOR_MODE_KEY));
  }
}

/**
 * Get the current setting > reverses it > stores it
 */
const toggleSetting = () => {
  let currentSetting = localStorage.getItem(STORAGE_KEY);
  
  switch(currentSetting) {
    case null:
      currentSetting = getCSSCustomProp(COLOR_MODE_KEY) === 'dark' ? 'light' : 'dark';
      break;
    case 'light':
      currentSetting = 'dark';
      break;
    case 'dark':
      currentSetting = 'light';
      break;
  }

  const toggledOn =
  'true' === buttonToggle.getAttribute( 'aria-checked' ) || false;
  buttonToggle.setAttribute( 'aria-checked', ! toggledOn );

  localStorage.setItem(STORAGE_KEY, currentSetting);
  
  return currentSetting;
}

/**
 * A shared method for setting the button text label and visually hidden status element 
 */
const setButtonLabelAndStatus = currentSetting => { 
	buttonToggleLabel.innerText = `Switch to ${currentSetting === 'dark' ? 'light' : 'night'} mode`;
  	buttonToggleNotice.innerText = `Colour mode is now "${currentSetting}"`;
}

/**
 * Clicking the button runs the apply setting method which grabs its parameter
 * from the toggle setting method.
 */
buttonToggle.addEventListener('click', evt => {
  evt.preventDefault();
  
  applySetting(toggleSetting());
});

applySetting();

/* https://codepen.io/heydon/pen/veeaEa */
/* Currently using compiled JS here; replace when JS build tools set up */
(function () {
	// Get relevant elements and collections
	const hasTabs = document.querySelector('.tabbed') !== null;

	if ( hasTabs ) {
		const tabbed = document.querySelector('.tabbed');
		const tablist = tabbed.querySelector('ul');
		const tabs = tablist.querySelectorAll('a');
		const panels = tabbed.querySelectorAll('[id^="project"]');
	
		// The tab switching function
		const switchTab = (oldTab, newTab) => {
		newTab.focus();
		// Make the active tab focusable by the user (Tab key)
		newTab.removeAttribute('tabindex');
		// Set the selected state
		newTab.setAttribute('aria-selected', 'true');
		oldTab.removeAttribute('aria-selected');
		oldTab.setAttribute('tabindex', '-1');
		// Get the indices of the new and old tabs to find the correct
		// tab panels to show and hide
		let index = Array.prototype.indexOf.call(tabs, newTab);
		let oldIndex = Array.prototype.indexOf.call(tabs, oldTab);
		panels[oldIndex].hidden = true;
		panels[index].hidden = false;
		};
	
		// Add the tablist role to the first <ul> in the .tabbed container
		tablist.setAttribute('role', 'tablist');
	
		// Add semantics are remove user focusability for each tab
		Array.prototype.forEach.call(tabs, (tab, i) => {
		tab.setAttribute('role', 'tab');
		tab.setAttribute('id', 'tab' + (i + 1));
		tab.setAttribute('tabindex', '-1');
		tab.parentNode.setAttribute('role', 'presentation');
	
		// Handle clicking of tabs for mouse users
		tab.addEventListener('click', e => {
			e.preventDefault();
			let currentTab = tablist.querySelector('[aria-selected]');
			if (e.currentTarget !== currentTab) {
			switchTab(currentTab, e.currentTarget);
			}
		});
	
		// Handle keydown events for keyboard users
		tab.addEventListener('keydown', e => {
			// Get the index of the current tab in the tabs node list
			let index = Array.prototype.indexOf.call(tabs, e.currentTarget);
			// Work out which key the user is pressing and
			// Calculate the new tab's index where appropriate
			let dir = e.which === 38 ? index - 1 : e.which === 40 ? index + 1 : e.which === 39 ? 'down' : null;
			if (dir !== null) {
			e.preventDefault();
			// If the down key is pressed, move focus to the open panel,
			// otherwise switch to the adjacent tab
			dir === 'down' ? panels[i].focus() : tabs[dir] ? switchTab(e.currentTarget, tabs[dir]) : void 0;
			}
		});
		});
	
		// Add tab panel semantics and hide them all
		Array.prototype.forEach.call(panels, (panel, i) => {
		panel.setAttribute('role', 'tabpanel');
		panel.setAttribute('tabindex', '-1');
		let id = panel.getAttribute('id');
		panel.setAttribute('aria-labelledby', tabs[i].id);
		panel.hidden = true;
		});
	
		// Set the default tab and panel
		tabs[0].removeAttribute('tabindex');
		tabs[0].setAttribute('aria-selected', 'true');
		panels[0].hidden = false;
	}
})();

/**
 * Sparkle FX
 * 
 * Run sparkles animation if in view.
 */
const sparkles = document.getElementById('js-sparkles');
const watcher = new IntersectionObserver( text => {
	let showSparkles;

	if ( 0 !== text[0].intersectionRatio ) {
		showSparkles = true;
		runSparkles(showSparkles);
	} else {
		showSparkles = null;
		runSparkles(showSparkles);
	}
});

if (null !== sparkles) {
	watcher.observe(sparkles);
}

/**
 * Sparkle generator
 */
const runSparkles = (hasSparkles) => {
	let refresh = 0;
	const sometimesIDoubtYourCommitmentToSparkleMotion = window.matchMedia('(prefers-reduced-motion: reduce)');

	if (true === hasSparkles) {
		running = hasSparkles;
	} else {
		running = null;
	}

	const sparkleMotion = now => {
		if ( ! refresh || now - refresh >= randomNumberFromRange(250,1000)) {
			refresh = now

			if (false === sometimesIDoubtYourCommitmentToSparkleMotion.matches) {
				insertSparkle();
			}

			// Manage sparkle lifecycle.
			const sparkles = document.querySelectorAll('.sparkle');

			sparkles.forEach((sparkle) => {
				sparkle.addEventListener('animationend', () => {
					sparkle.remove();
				})
			})
		}

		if (true === running) {
			window.requestAnimationFrame(sparkleMotion);
		} else {
			window.cancelAnimationFrame(sparkleMotion);			
		}
	}

	window.requestAnimationFrame(sparkleMotion);

	function insertSparkle() {
		const sparkleTarget = document.getElementById('js-sparkles');
		sparkleTarget.insertAdjacentHTML('afterbegin', makeSparkle());
	}

	function makeSparkle() {
		let size = randomNumberFromRange(10,20);
		const star = '<span class="sparkle" style="left: ' + randomPositionX() + '%; top: ' + randomPositionY() + '%;"><svg style="height: ' + size +'px; width: ' + size +'px;" class="sparkle-star" aria-hidden="true" viewBox="0 0 160 160" xmlns="http://www.w3.org/2000/svg"><path d="M80 0C80 0 84.2846 41.2925 101.496 58.504C118.707 75.7154 160 80 160 80C160 80 118.707 84.2846 101.496 101.496C84.2846 118.707 80 160 80 160C80 160 75.7154 118.707 58.504 101.496C41.2925 84.2846 0 80 0 80C0 80 41.2925 75.7154 58.504 58.504C75.7154 41.2925 80 0 80 0Z" /></svg></span>';

		return star;
	}

	function randomPositionX() {
		return Math.floor(Math.random() * Math.floor(90));
	}

	function randomPositionY() {
		return Math.floor(Math.random() * Math.floor(50));
	}

	function randomNumberFromRange(low, high) {
		min = Math.ceil(low);
		max = Math.floor(high);

		return Math.floor(Math.random() * (max - min + 1) + min);
	}
};

/**
 * X-ray FX
 */
( () => {
	const hasXray = document.getElementById('js-x-ray') !== null;

	if ( hasXray ) {
		const selfie = document.querySelector('.js-icon-selfie');
		const scope = document.querySelector('.js-x-ray-scope');
		const svgPoint = selfie.createSVGPoint();

		document.addEventListener('mousemove', e => {
			setScopeCoords(getInputCoords(e, selfie));
		}, false);

		document.addEventListener('touchmove', e => {
			e.preventDefault();
			let touched = e.targetTouches[0];

			if (touched) {
				setScopeCoords(getInputCoords(touched, selfie));
			}
		}, false);

		let getInputCoords = (e, svg) => {
			svgPoint.x = e.clientX;
			svgPoint.y = e.clientY;

			return svgPoint.matrixTransform(svg.getScreenCTM().inverse());
		}

		let setScopeCoords = coordinates => {
			scope.setAttribute('cx', coordinates.x);
			scope.setAttribute('cy', coordinates.y);
		}
	}
})();